import 'package:ems_mob_app/config/dev_conf.dart';
import 'package:ems_mob_app/config/test_conf.dart';
import 'package:ems_mob_app/config/prod_conf.dart';

class Env {

  static Map conf = DevEnvironment.config;
  // static Map conf = ProdEnvironment.config;
  // static Map conf = TestEnvironment.config;
}