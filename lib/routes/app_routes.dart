part of 'app_pages.dart';

abstract class Routes {

  // Default Pages
  static const LOADING = '/loading';
  static const LOGIN = '/login';
  static const SIGNUP = '/signup';
  static const FORGOT_PASSWORD = '/forgotPassword';
  static const FORGOT_PASSWORD_FEEDBACK = '/forgotPasswordFeedback';
  static const DASHBOARD = '/dashboard';
  static const APP_INFO = '/appInfo';
  static const PROFILE = '/profile';
  static const SETTINGS = '/settings';
  static const NOTIFICATIONS = '/notifications';
  static const PREFERENCES = '/preferences';
  static const SINGLE_VISITOR = '/singleVisitor';
  static const CHANGE_PASSWORD = '/changePassword';
  static const HELP_AND_SUPPORT = '/helpAndSupport';

  // Application Pages
  static const LOCATION_SEARCH = '/locationSearch';
  static const ALL_SEARCH = '/allSearch';
  static const SEARCH_RESULT = '/searchResult';
  static const NOTEPAD = '/notepad';
  static const SINGLE_SHUTTLE_BOOKING = '/singleShuttleBooking';
}
