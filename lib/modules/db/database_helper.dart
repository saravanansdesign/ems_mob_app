import 'dart:io';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class SQFLite{

  static const _dbName = "YesLedgers.db";
  static const _dbVersion = 1;

  //User Table
  static const _tableUser = "users";
  static const userId = "_id TEXT";
  static const userEmail = "username TEXT";
  static const userPassword= "password TEXT";

  //Notes Table
  static const _tableNotes = "notes";
  static const noteId = "_id TEXT";
  static const noteTitle = "noteTitle TEXT";
  static const noteContent= "noteContent TEXT";

  late Database _database;

  Future<Database> get database async{
    if(_database != null){
      return _database;
    }else{
      _database = await _initiateDatabase();
      return _database;
    }
  }

  _initiateDatabase() async{
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, _dbName);

    return await openDatabase(path, version: _dbVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async{

    //Users Table
    db.query(
      '''
      CREATE  TABLE $_tableUser(
      $userId PRIMARY KEY,
      $userEmail NOT NULL,
      $userPassword NOT NULL)
      '''
    );

    //Notes Table
    db.query(
      '''
      CREATE  TABLE $_tableNotes(
      $noteId PRIMARY KEY,
      $noteTitle NOT NULL,
      $noteContent NOT NULL)
      '''
    );
  }

  Future insert(obj) async {
    obj.id = await _database.insert(_tableNotes, obj.toMap());
    return obj;
  }

  Future getTodo(int id) async {
    List<Map> maps = await _database.query(_tableNotes,
        columns: [noteId],
        where: '$noteId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return maps;
    }
    return null;
  }

  Future<int> delete(int id) async {
    return await _database.delete(_tableNotes, where: '$noteId = ?', whereArgs: [id]);
  }

  Future<int> update(obj) async {
    return await _database.update(_tableNotes, obj.toMap(),
        where: '$noteId = ?', whereArgs: [obj.id]);
  }
}