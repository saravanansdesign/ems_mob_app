import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:intl/intl.dart';

class UtilService {

  @override
  void initState() {
  }

  Future<bool> connectivityCheck() async{
    var connectivityResult = await (Connectivity().checkConnectivity());
    bool connStatus = false;
    if (connectivityResult != ConnectivityResult.none) {
      connStatus = true;
    }
    return connStatus;
  }

  void fToast(msg, align){

    var type = ToastGravity.CENTER;

    if(align == "center"){
      type = ToastGravity.CENTER;
    }else if(align == "top"){
      type = ToastGravity.TOP;
    }else if(align == "bottom"){
      type = ToastGravity.BOTTOM;
    }

    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: type,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }



  Map<String, Object> searchResultFormatter(data) {

    var resultObj = {
      "total": 0,
      "data": {},
      "aggregations": {}
    };

    if (data["httpCode"] == 200) {

      var arrayData = jsonDecode(data["result"]);
      var totalRecords = arrayData["hits"]["total"]["value"];
      var records = arrayData["hits"]["hits"];

      var aggregations = {};

      if(arrayData["aggregations"] != null){
        aggregations = arrayData["aggregations"];
      }

      List<dynamic> list = [];
      for (var i = 0; i < records.length; i++) {
        records[i]['_source']['_id'] = records[i]['_id'];
        list.insert(i, records[i]['_source']);
      }

      resultObj = {
        "total": totalRecords,
        "data": {
          "recordsTotal": totalRecords,
          "recordsFiltered": totalRecords,
          "data": list
        },
        "aggregations": aggregations
      };

      return resultObj;

    } else {
      return resultObj;
    }
  }

  void sweetAlert(alertType,context,heading,subTitle) async{

    if(alertType == "success"){
      ArtSweetAlert.show(
          context: context,
          artDialogArgs: ArtDialogArgs(
              type: ArtSweetAlertType.success,
              barrierColor: hexColor("#eeeeee"),
              title: (heading != null) ? heading : "Success",
              confirmButtonText: "Ok, Close",
              confirmButtonColor: hexColor("#027ad6"),
              text: subTitle
          )
      );

    }else if(alertType == "error"){

      ArtSweetAlert.show(
          context: context,
          artDialogArgs: ArtDialogArgs(
              type: ArtSweetAlertType.danger,
              barrierColor: hexColor("#eeeeee"),
              title: (heading != null) ? heading : "Failed!",
              confirmButtonText: "Ok, Close",
              confirmButtonColor: hexColor("#027ad6"),
              text: subTitle
          )
      );

    }else if(alertType == "info"){

      ArtSweetAlert.show(
          context: context,
          artDialogArgs: ArtDialogArgs(
              type: ArtSweetAlertType.info,
              barrierColor: hexColor("#eeeeee"),
              title: (heading != null) ? heading : "Info",
              confirmButtonText: "Ok, Close",
              confirmButtonColor: hexColor("#027ad6"),
              text: subTitle
          )
      );

    }else if(alertType == "confirmation"){

      ArtDialogResponse response = await ArtSweetAlert.show(
          barrierDismissible: false,
          context: context,
          artDialogArgs: ArtDialogArgs(
              denyButtonText: "Cancel",
              title: (heading != null) ? heading : "Are you sure?",
              text: subTitle,
              barrierColor: hexColor("#eeeeee"),
              confirmButtonColor: hexColor("#027ad6"),
              confirmButtonText: "Yes",
              type: ArtSweetAlertType.warning
          )
      );

      if(response==null) {
        return;
      }

      if(response.isTapConfirmButton) {
        ArtSweetAlert.show(
            context: context,
            artDialogArgs: ArtDialogArgs(
                type: ArtSweetAlertType.success,
                title: "Confirmed!"
            )
        );
        return;
      }
    }
  }

  dhzCapitalize(text){
    var res = text[0].toUpperCase() + text.substring(1).toLowerCase();
    return res;
  }

  hexColor(color){

    switch (color.runtimeType) {
      case String:
      // do something
        return Color(int.parse(color.replaceAll('#', '0xff')));
        break;
      case Null:
      // do something else
        return Color(int.parse("#ffffff".replaceAll('#', '0xff')));
        break;
    }
  }

  dhzTime(){

    var now = new DateTime.now();
    var formatter = new DateFormat('MM/dd/yyyy');
    String today = formatter.format(now);
    DateFormat dateFormat = DateFormat("MM/dd/yyyy, HH:mm:ss a");
    DateTime dateTime = dateFormat.parse("${today}, 00:00:00 AM");
    DateTime dateTime2 = dateFormat.parse("${today}, 12:00:00 PM");
    // var d12 = DateFormat('MM/dd/yyyy, HH:mm:ss a').format(dateTime);

    var todayTS = {
      "fromTime" : dateTime.toUtc().millisecondsSinceEpoch,
      "toTime" : dateTime2.toUtc().millisecondsSinceEpoch
    };

    return todayTS;
  }

  dhzMillis(){
    return DateTime.now().millisecondsSinceEpoch;
  }

  dhzTStoDate(timestamp){

    if(timestamp != "" && timestamp != null){
      timestamp = timestamp;

      var dt = DateTime.fromMillisecondsSinceEpoch(timestamp);
      var d12 = DateFormat('dd/MM/yyyy').format(dt); // 12/31/2000, 10:00 PM
      return d12;
    }else{
      return null;
    }
  }

  dhzTStoDateTime(timestamp){

    if(timestamp != "" && timestamp != null){
      timestamp = timestamp;

      var dt = DateTime.fromMillisecondsSinceEpoch(timestamp);
      var d12 = DateFormat('dd/MM/yyyy hh:m a').format(dt); // 12/31/2000, 10:00 PM
      return d12;
    }else{
      return null;
    }
  }

  Future<void> openBrowserTab(String link) async {
    await FlutterWebBrowser.openWebPage(url: link);
  }

  getCap(String text) {
    List arrayPieces = [];

    String outPut = '';

    text.split(' ').forEach((sepparetedWord) {
      arrayPieces.add(sepparetedWord);
    });

    arrayPieces.forEach((word) {
      word =
      "${word[0].toString().toUpperCase()}${word.toString().substring(1)} ";
      outPut += word;
    });

    return outPut;
  }

  isNull(text){
    return (text != null);
  }

  isEmptyCheck(String text){
    return (text != "");
  }

  isEmptyReplace(text){

    if(text != null){
      return (text != "" ? text : "");
    }else{
      return "";
    }
  }

/*Future<bool> backButtonAction(context) async{
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(false),
            child: Text("NO"),
          ),
          SizedBox(height: 16),
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(true),
            child: Text("YES"),
          ),
        ],
      ),
    ) ??
        false;
  }*/
}
