import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:get/get.dart';
import 'package:ems_mob_app/modules/utils/constant_variables.dart';
import 'package:ems_mob_app/services/auth_api.dart';

List evList = [];

class ExtraVisitorPage extends StatefulWidget {
  const ExtraVisitorPage({Key? key}) : super(key: key);

  @override
  _ExtraVisitorPageState createState() => _ExtraVisitorPageState();
}

class _ExtraVisitorPageState extends State<ExtraVisitorPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  UtilService utils = UtilService();
  AuthApiService getVisitors = AuthApiService();

  List visitorList = [];
  List visitorSearchList = [];

  void initState() {
    super.initState();

    if (evList.length == 0) {
      _addCardWidget();
    }

    for (int i = 0; i < evList.length; i++) {
      _addCardWidget();
      visitorList[i]["evNameCtrl"].text = evList[i]["vname"];
      visitorList[i]["evMobileCtrl"].text = evList[i]["mobile"];
      visitorList[i]["evMailCtrl"].text = evList[i]["vemail"];
    }
  }

  void _addCardWidget() {
    setState(() {
      var obj = {
        "evNameCtrl": TextEditingController(),
        "evMailCtrl": TextEditingController(),
        "evMobileCtrl": TextEditingController()
      };
      visitorList.add(obj);
    });
  }

  Widget _card(index) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(10, 10, 10, 18),
          decoration: BoxDecoration(
            color: utils.hexColor("#ffffff"),
            border: Border.all(
              width: 1,
              color: utils.hexColor("#dddddd"),
            ),
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                color: utils.hexColor("#dddddd"),
                offset: Offset(0.0, 3.0),
              ),
            ],
          ),
          margin: EdgeInsets.symmetric(horizontal: 25.0, vertical: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: TextFormField(
                  controller: visitorList[index]["evNameCtrl"],
                  maxLength: 50,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp("[a-z A-Z]")),
                  ],
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Enter Your Extra Visitor\'s Name';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    print(value);
                    if (value != "") {
                        getVisitors.visitorList(value, false).then((result) async {
                          var resJson = json.decode(result["result"]["result"]);
                          setState(() {
                            visitorSearchList = resJson["hits"]["hits"].toList() ?? [];
                          });
                        });
                    } else {
                      setState(() {
                        visitorSearchList = [];
                      });
                    }
                  },
                  decoration: const InputDecoration(
                    labelText: 'Visitor Name *',
                    contentPadding: EdgeInsets.only(
                        left: 15, bottom: 11, top: 11, right: 15),
                    border: OutlineInputBorder(),
                    counterText: "",
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: TextFormField(
                  controller: visitorList[index]["evMobileCtrl"],
                  maxLength: 10,
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                    } else if (value.length != 10) {
                      return 'Enter 10 Digit Mobile Number';
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    labelText: 'Mobile Number',
                    contentPadding: EdgeInsets.only(
                        left: 15, bottom: 11, top: 11, right: 15),
                    border: OutlineInputBorder(),
                    counterText: "",
                  ),
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                  ],
                  keyboardType: TextInputType.number,
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: TextFormField(
                  controller: visitorList[index]["evMailCtrl"],
                  keyboardType: TextInputType.emailAddress,
                  maxLength: 50,
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                    } else {
                      bool emailValid = RegExp(
                          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                          .hasMatch(value);
                      if (!emailValid) {
                        return 'Enter valid email';
                      }
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    labelText: 'Email ID',
                    contentPadding: EdgeInsets.only(
                        left: 15, bottom: 11, top: 11, right: 15),
                    border: OutlineInputBorder(),
                    counterText: "",
                  ),
                ),
              ),
            ],
          ),
        ),
        if (visitorList[index]["evNameCtrl"].text.length > 0 &&
            visitorSearchList.length > 0)
          Container(
            height: 250,
            margin: EdgeInsets.fromLTRB(30, 85, 30, 0),
            color: Colors.white,
            child: ListView.builder(
                itemCount: visitorSearchList.length,
                itemBuilder: (b, i) {
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        visitorList[index]["evNameCtrl"].text =
                        visitorSearchList[i]["_source"]["vname"];
                        visitorList[index]["evMobileCtrl"].text =
                        visitorSearchList[i]["_source"]["mobile"];
                        visitorList[index]["evMailCtrl"].text =
                        visitorSearchList[i]["_source"]["vemail"];
                        visitorSearchList = [];
                      });
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            utils.getCap("boodskap"),
                            style: TextStyle(fontWeight: FontWeight.bold,fontFamily: AppTheme.font,),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text(visitorSearchList[i]["_source"]
                                    ["vmobile"])),
                                Text(utils.getCap(visitorSearchList[i]["_source"]
                                ["vorganization"]))
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
          ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        backgroundColor: AppTheme.primary,
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context)),
        title: Text(
          "Extra Visitors",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: visitorList.length,
                  itemBuilder: (context, index) {
                    return _card(index);
                  }),

              Padding(
                padding: EdgeInsets.only(top: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    MaterialButton(
                      height: 50.0,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                      elevation: 3,
                      color: utils.hexColor("#ffffff"),
                      child: Padding(
                        padding: EdgeInsets.all(4.0),
                        child:Row(
                          children: [
                            Container(
                              child: Icon(Icons.add,size: 21,color: utils.hexColor("#363636"),),
                            ),
                            Text('Add More Visitor', style: TextStyle(fontSize: 15,color: utils.hexColor("#363636")),),],
                        ),
                      ),
                      onPressed: () {
                        visitorSearchList = [];
                        if (_formKey.currentState!.validate()) {
                          _addCardWidget();
                        }
                      },
                    ),
                    MaterialButton(
                      height: 50.0,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                      elevation: 3,
                      color: AppTheme.primary,
                      child: Padding(
                        padding: EdgeInsets.all(4.0),
                        child:Row(
                          children: [
                            Container(
                              child: const Icon(Icons.check,size: 21,color: Colors.white,),
                            ),
                            const Text('Confirm', style: TextStyle(fontSize: 15,color: Colors.white),),],
                        ),
                      ),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          evList = [];
                          // evList.add(
                          //   {
                          //     "vname": extraVisName.text,
                          //     "mobile": extraVisMobile.text == "" ? "" : extraVisMobile.text,
                          //     "vmobile": extraVisMobile.text == "" ? "" : "+91" + extraVisMobile.text,
                          //     "vemail": extraVisMail.text == "" ? "" : extraVisMail.text,
                          //     "vphoto": "",
                          //     "badgeid":"",
                          //     "isvisted": false,
                          //   },
                          // );
                          for (int i = 0; i < visitorList.length; i++) {
                            evList.add(
                              {
                                "vname": visitorList[i]["evNameCtrl"].text,
                                "mobile":
                                visitorList[i]["evMobileCtrl"].text == ""
                                    ? ""
                                    : visitorList[i]["evMobileCtrl"].text,
                                "vmobile":
                                visitorList[i]["evMobileCtrl"].text == ""
                                    ? ""
                                    : "+91" +
                                    visitorList[i]["evMobileCtrl"].text,
                                "vemail":
                                visitorList[i]["evMailCtrl"].text == ""
                                    ? ""
                                    : visitorList[i]["evMailCtrl"].text,
                                "vphoto": "",
                                "badgeid": "",
                                "isvisted": false,
                              },
                            );
                          }
                          Get.back(result: evList);
                          // Navigator.pop(context);

                          // Get.toNamed(AppPages.createSchedule);
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
