import 'package:flutter/material.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ems_mob_app/modules/utils/constant_variables.dart';
import 'package:get/get.dart';

import '../../../controllers/entry_controller.dart';

class SearchBoxPage extends StatefulWidget {
  const SearchBoxPage({Key? key}) : super(key: key);

  @override
  _SearchBoxPageState createState() => _SearchBoxPageState();
}

class _SearchBoxPageState extends State<SearchBoxPage> {

  UtilService utils = UtilService();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  EntryController entryCtrl = EntryController();

  var singleResObj;
  List searchResult = [];
  dynamic resultList = [];
  Map? userObj;
  String? searchSingle;
  TextEditingController SearchBoxCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: utils.hexColor("#363636")),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: TextField(
          controller: SearchBoxCtrl,
          obscureText: false,
          decoration: InputDecoration(
            hintText: "Search Your Ledger Here",
            hintStyle: TextStyle(fontSize: 18, color: utils.hexColor("#aaaaaa")),
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            contentPadding: const EdgeInsets.only(left: 0, bottom: 11, top: 11, right: 15),
          ),
        ),
        backgroundColor: AppTheme.lightBackground,
      ),
      body: SingleChildScrollView(
        child:  Container(
          margin: EdgeInsets.all(0.0),
          padding: EdgeInsets.all(0.0),
          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          color: utils.hexColor("#d6d6d6"),
          child: _renderSingles(),
      ),
      ),
    );
  }

  Widget _renderSingles(){
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: resultList.length,
      itemBuilder: (build, index) {
        return GestureDetector(
          child: Container(
            width: MediaQuery.of(context).size.width-10,
            padding: const EdgeInsets.all(10.0),
            decoration: BoxDecoration(
                color: utils.hexColor("#f6f6f6"),
                border: Border(
                    bottom: BorderSide(
                        color: utils.hexColor("#dddddd"),
                        width: 1.0
                    )
                )
            ),
            child: Text(resultList[index]["ledger_name"], style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),),
          ),
          onTap: (){
            singleResObj = resultList[index];
            if(utils.isNull(singleResObj)){
              SearchBoxCtrl.text = singleResObj["ledger_name"];
            }
            Get.back(result: resultList[index]);
          },
        );
      },
    );
  }

  Future<void> getLedgersList() async {
    var res = await entryCtrl.getAllLedgersList();
    print("getLedgersList----------");
    print(res);
    if(res["status"]){
      setState(() {
        resultList = res["data"];
      });
    }
  }

  @override
  void initState() {
    singleResObj = Get.arguments;

    getLedgersList();
    if(utils.isNull(singleResObj)){
      // SearchBoxCtrl.text = singleResObj["ledger_name"];
    }
  }
}
