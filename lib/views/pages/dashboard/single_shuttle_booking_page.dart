import 'package:flutter/material.dart';
import 'package:ems_mob_app/controllers/shuttle_controller.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ems_mob_app/modules/utils/constant_variables.dart';
import 'package:get/get.dart';
import 'package:ems_mob_app/routes/app_pages.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class SingleShuttleBookingPage extends StatefulWidget {
  const SingleShuttleBookingPage({Key? key}) : super(key: key);

  @override
  _SingleShuttleBookingPageState createState() => _SingleShuttleBookingPageState();
}

class _SingleShuttleBookingPageState extends State<SingleShuttleBookingPage> {

  UtilService utils = UtilService();
  ShuttleController shuttleCtrl = ShuttleController();

  var selectedShuttleObj;
  List searchResult = [];
  List seatsList = [];
  Map? userObj;
  Map? selectedShuttle;
  String? searchLocation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#eeeeee"),
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: AppTheme.primary),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: Icon(FontAwesomeIcons.ticketAlt, color: AppTheme.primary,),
            ),
            Text("Seat Booking", style: TextStyle(color: utils.hexColor("#363636")),)
          ],
        ),
        backgroundColor: AppTheme.lightBackground,
        bottom: PreferredSize(
            child: Container(
              color: AppTheme.secondary,
              height: 4.0,
            ),
            preferredSize: const Size.fromHeight(4.0)),
      ),
      body: SingleChildScrollView(
        child:  Column(
          children: [
            _renderTripDetails(),
            _renderSeats()
          ],
        ),
      ),
    );
  }
  
  Widget _renderTripDetails(){
    return Container(
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: utils.hexColor("#ffffff"),
        border : Border.all(
          color: utils.hexColor("#dddddd"),
          width: 1.0,
        )
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              Padding(
                padding: EdgeInsets.only(right: 5.0),
                child: FaIcon(FontAwesomeIcons.mapMarkerAlt, size: 16.0, color: AppTheme.primary,),
              ),
              Text("${selectedShuttleObj["from_loc"]}",
                style: TextStyle(color: utils.hexColor("#464646"), fontSize: 16.0, fontWeight: FontWeight.w800),),
              Text(" to ",
                style: TextStyle(color: utils.hexColor("#464646"), fontSize: 16.0),),
              Text("${selectedShuttleObj["to_loc"]}",
                style: TextStyle(color: utils.hexColor("#464646"), fontSize: 16.0, fontWeight: FontWeight.w800),)
            ],
          ),

          Padding(padding: EdgeInsets.only(top:10.0), child: Row(
            children: [
              Padding(
                padding: EdgeInsets.only(right: 5.0),
                child: FaIcon(FontAwesomeIcons.clock, size: 16.0, color: AppTheme.primary,),
              ),
              Text("Travel : ", style: TextStyle(color: utils.hexColor("#464646"), fontSize: 16.0),),
              Text("${selectedShuttleObj["trip_start_time"]}",
                style: TextStyle(color: utils.hexColor("#464646"), fontSize: 16.0, fontWeight: FontWeight.w800),),
              Text(" to ",
                style: TextStyle(color: utils.hexColor("#464646"), fontSize: 16.0),),
              Text("${selectedShuttleObj["trip_end_time"]}",
                style: TextStyle(color: utils.hexColor("#464646"), fontSize: 16.0, fontWeight: FontWeight.w800),)
            ],
          ),)
        ],
      ),
    );
  }

  Widget _renderSeats(){
    return Container(
      height: MediaQuery.of(context).size.height - 100,
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: seatsList.length,
        itemBuilder: (build, index) {
          return GestureDetector(
            child: Row(
              children: [
                Container(
                  height: 50,
                  width: 50,
                  padding: const EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                      color: utils.hexColor("#ffffff"),
                      border: Border(
                          bottom: BorderSide(
                              color: utils.hexColor("#dddddd"),
                              width: 1.0
                          )
                      )
                  ),
                  child: Text("${seatsList[index]["seat_no"]}"),
                )
              ],
            ),
            onTap: (){
              selectedShuttle = seatsList[index];
              Get.toNamed(AppPages.singleShuttleBooking, arguments: selectedShuttle);
            },
          );
        },
      ),
    );
  }


  Widget selectedChair(int a , int b){
    return
      InkWell(
        onTap: (){

        },
        child: Container(
          decoration: BoxDecoration(
              color: Colors.yellow,
              borderRadius: BorderRadius.circular(3.0)
          ),
        ),
      );
  }

  Widget availableChair(int a, int b){
    return InkWell(
      onTap: (){

      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3.0),
          border : Border.all(
            color: Color.fromRGBO(0, 0, 0, 1),
            width: 1,
          ),
        ),
      ),
    );
  }

  Widget reservedChair(){
    return Container(
      decoration: BoxDecoration(
          color: Color.fromRGBO(15, 15, 15, 0.24),
          borderRadius: BorderRadius.circular(3.0)
      ),
    );
  }

  void getSingleShuttleDetails() async{
    EasyLoading.show(status: 'loading...',maskType: EasyLoadingMaskType.black);
    var res = await shuttleCtrl.getSingleShuttleDetails(selectedShuttleObj);
    EasyLoading.dismiss();

    if(res["status"]){
       setState(() {
         seatsList = res["result"];
         print("seatsList------------------");
         print(seatsList);
       });
     }else{
       setState(() {
         seatsList = [];
       });
     }
  }

  @override
  void initState() {
    selectedShuttleObj = Get.arguments;
    getSingleShuttleDetails();
  }
}
