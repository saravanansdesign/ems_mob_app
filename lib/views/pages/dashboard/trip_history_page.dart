import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:ems_mob_app/controllers/shuttle_controller.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:get/get.dart';
import 'package:ems_mob_app/modules/utils/constant_variables.dart';
import 'package:ems_mob_app/routes/app_pages.dart';
import 'package:ems_mob_app/services/auth_api.dart';
import 'package:html_editor_enhanced/html_editor.dart';

class TripHistoryPage extends StatefulWidget {
  const TripHistoryPage({Key? key}) : super(key: key);

  @override
  _TripHistoryPageState createState() => _TripHistoryPageState();
}

class _TripHistoryPageState extends State<TripHistoryPage> {

  UtilService utils = UtilService();
  AuthApiService authService = AuthApiService();
  ShuttleController shuttleCtrl = ShuttleController();
  Map? customerObj;
  final HtmlEditorController htmlCtrl = HtmlEditorController();
  var tripHistory = [];
  var tripHistoryCount;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        backgroundColor: AppTheme.primary,
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () =>  Navigator.pop(context)
        ),
        title: Text('Shuttle History (${ utils.isNull(tripHistoryCount) ? tripHistoryCount : "0" })'),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              getMyTripHistoryList();
            },
            child: const Padding(
              padding:EdgeInsets.all(14),
              child: Icon(
                Icons.refresh,
                size:30,
              ),
            ),
          ),
        ],
      ),
      body: HtmlEditor(
        controller: htmlCtrl,
        htmlEditorOptions: const HtmlEditorOptions(
        hint: 'Your text here...',
        shouldEnsureVisible: true,
        //initialText: "<p>text content initial, if any</p>",
        ),
        ),
    );
  }

  Future<void> getMyTripHistoryList() async{

    /*EasyLoading.show(status: 'loading...',maskType: EasyLoadingMaskType.black);
    var res = await shuttleCtrl.getShuttlesHistoryList();
    EasyLoading.dismiss();
    setState(() {
      tripHistory = res["result"]["data"]["data"];
      tripHistoryCount = res["result"]["data"]["recordsTotal"];
    });*/
  }

  Future<void> getUserDetails() async{
    var customer = await authService.getCustomerObj();
    setState(() {
      customerObj = customer;
    });
  }

  @override
  void initState() {
    super.initState();
    getUserDetails();
    // getMyTripHistoryList();
  }
}