import 'package:ems_mob_app/controllers/entry_controller.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:ems_mob_app/app-conf.dart';
import 'package:ems_mob_app/modules/utils/constant_variables.dart';
import 'package:ems_mob_app/services/auth_api.dart';
import 'package:ems_mob_app/controllers/dashboard_controller.dart';
import 'package:ems_mob_app/modules/utils/dhz_times.dart';
import 'package:get/get.dart';
import '../../../routes/app_pages.dart';

var clientSessionId = "WEB_${DateTime.now().millisecondsSinceEpoch}";
final client =
    MqttServerClient('wss://${Env.conf["MQTT_BASE"]}', clientSessionId);

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {

  Dhs dhs = Dhs();
  UtilService utils = UtilService();
  AuthApiService authService = AuthApiService();

  EntryController entryCtrl = EntryController();

  var balanceAmount = 0;
  var totalIncome = 0;
  var totalExpense = 0;

  var paidDate;
  var isChecked = false;
  String dropdownValue = 'One';
  var LEDGERS_ARR = ['One', 'Two', 'Free', 'Four'];

  var userObj;
  var LOGO_BRANDING_URL = Image.asset('assets/images/logo.png', height: 24.0);
  var profileImgSet = Image.asset(
    'assets/images/user.png',
    fit: BoxFit.cover,
  );

  dynamic entryListArr = [];
  static const TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  int _selectedIndex = 0;

  final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return await showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: const Text('Are you sure?'),
                content: const Text('Do you want to exit an App'),
                actions: <Widget>[
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(false),
                    child: const Padding(
                        padding: EdgeInsets.all(10.0), child: Text("No")),
                  ),
                  const SizedBox(height: 16),
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(true),
                    child: const Padding(
                        padding: EdgeInsets.all(10.0), child: Text("Yes")),
                  ),
                ],
              ),
            ) ??
            false;
      },
      child: Scaffold(
        key: _key,
        backgroundColor: utils.hexColor("#f1f1f1"),
        appBar: AppBar(
          leading: IconButton(
            icon: new Icon(
              Icons.menu,
              color: utils.hexColor("#363636"),
            ),
            onPressed: () {
              _key.currentState?.openDrawer();
            },
          ),
          backgroundColor: AppTheme.lightBackground,
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(right: 10.0),
                  child: Image.asset(
                      "assets/images/yesledegers-logo.png",
                      height: 27.0),
                ),
              ),
              Row(
                children: [
                  Text.rich(
                    TextSpan(
                      text: 'YES',
                      style: TextStyle(fontSize: 18, color: AppTheme.secondary, fontWeight: FontWeight.w800, fontFamily: AppTheme.font),
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      text: 'LEDGERS',
                      style: TextStyle(fontSize: 18, color: AppTheme.primaryDark, fontWeight: FontWeight.w800, fontFamily: AppTheme.font),
                    ),
                  )
                ],
              )
            ],
          )
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              if(_selectedIndex == 0)
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _headerOverview(),
                    _renderEntryWidgets()
                  ],
                )
              else if(_selectedIndex == 1)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      child: Container(
                        decoration: BoxDecoration(
                          color: AppTheme.lightBackground,
                          border: Border.all(
                              width: 1.0, color: utils.hexColor("#dddddd")
                          ),
                        ),
                        margin: EdgeInsets.all(10.0),
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Choose Your Ledger", style: TextStyle(fontSize: 14.0, color: AppTheme.labelText),),
                                Padding(
                                  padding: EdgeInsets.only(top :5),
                                  child: Text("Ledger 1", style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600, color: utils.hexColor("#363636")),),
                                )
                              ],
                            ),
                            Expanded(child: Text("")),
                            FaIcon(FontAwesomeIcons.chevronRight, size: 20, color:AppTheme.greyText),
                          ],
                        ),
                      ),
                        onTap: (){
                          Get.toNamed(AppPages.allSearch);
                        },
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: [
                            SizedBox(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: TextFormField(
                                      textAlignVertical: TextAlignVertical.center,
                                      textAlign: TextAlign.left,
                                      controller: paidDate,
                                      style: TextStyle(color: AppTheme.darkColor, fontSize: 16.0),
                                      obscureText: false,
                                      validator: (String? value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Date is required';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        // contentPadding: EdgeInsets.all(20.0),
                                        hintText: 'Paid Date',
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                      ),
                                      onChanged: (val) {

                                      }),
                                )
                            ),
                            SizedBox(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: TextFormField(
                                      textAlignVertical: TextAlignVertical.center,
                                      textAlign: TextAlign.left,
                                      controller: paidDate,
                                      style: TextStyle(color: AppTheme.darkColor, fontSize: 16.0),
                                      obscureText: false,
                                      validator: (String? value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Time is required';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        // contentPadding: EdgeInsets.all(20.0),
                                        hintText: 'Paid Time',
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                      ),
                                      onChanged: (val) {

                                      }),
                                )),
                          ],
                        ),

                        Row(
                          children: [
                            SizedBox(
                                width: MediaQuery.of(context).size.width,
                                child: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: TextFormField(
                                      textAlignVertical: TextAlignVertical.center,
                                      textAlign: TextAlign.left,
                                      controller: paidDate,
                                      style: TextStyle(color: AppTheme.darkColor, fontSize: 16.0),
                                      obscureText: false,
                                      validator: (String? value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Amount is required';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        // contentPadding: EdgeInsets.all(20.0),
                                        hintText: 'Amount',
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                      ),
                                      onChanged: (val) {

                                      }),
                                )
                            ),
                          ],
                        ),

                        Row(
                          children: [
                            SizedBox(
                                width: MediaQuery.of(context).size.width,
                                child: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: TextFormField(
                                      textAlignVertical: TextAlignVertical.center,
                                      textAlign: TextAlign.left,
                                      controller: paidDate,
                                      style: TextStyle(color: AppTheme.darkColor, fontSize: 16.0),
                                      obscureText: false,
                                      validator: (String? value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Note is required';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        // contentPadding: EdgeInsets.all(20.0),
                                        hintText: 'Note',
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                      ),
                                      onChanged: (val) {

                                      }),
                                )
                            ),
                          ],
                        ),

                        Row(
                          children: [
                            SizedBox(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: TextFormField(
                                      textAlignVertical: TextAlignVertical.center,
                                      textAlign: TextAlign.left,
                                      controller: paidDate,
                                      style: TextStyle(color: AppTheme.darkColor, fontSize: 16.0),
                                      obscureText: false,
                                      validator: (String? value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Category is required';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        // contentPadding: EdgeInsets.all(20.0),
                                        hintText: 'Category',
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                      ),
                                      onChanged: (val) {

                                      }),
                                )
                            ),
                            SizedBox(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: TextFormField(
                                      textAlignVertical: TextAlignVertical.center,
                                      textAlign: TextAlign.left,
                                      controller: paidDate,
                                      style: TextStyle(color: AppTheme.darkColor, fontSize: 16.0),
                                      obscureText: false,
                                      validator: (String? value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Payment type is required';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        // contentPadding: EdgeInsets.all(20.0),
                                        hintText: 'Payment Type',
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                      ),
                                      onChanged: (val) {

                                      }),
                                )),
                          ],
                        ),

                        Row(
                          children: [
                            Checkbox(
                              checkColor: Colors.white,
                              value: isChecked,
                              onChanged: (bool? value) {
                                setState(() {
                                  isChecked = value!;
                                });
                              },
                            ),
                            Text("Use same date and time for my next entry", style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600, fontStyle: FontStyle.italic, color: utils.hexColor("#464646")),)
                          ],
                        ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(right: 7.0),
                                  child: SizedBox(
                                    height: 50.0,
                                    width: (MediaQuery.of(context).size.width / 2) - 30,
                                    child: ElevatedButton.icon(
                                        onPressed: () async {

                                        },
                                        label: const Text("Income"),
                                        icon: FaIcon(FontAwesomeIcons.signOutAlt, size: 20, color:AppTheme.solidBtnText),
                                        style: ElevatedButton.styleFrom(
                                          primary: AppTheme.secondary,
                                          onPrimary: AppTheme.solidBtnText,
                                          side: BorderSide(
                                              width: 1.0,
                                              color: AppTheme.secondary
                                          ),
                                          textStyle: TextStyle(
                                              fontWeight: FontWeight.w800,
                                              color: AppTheme.solidBtnText,
                                              fontSize: 16,
                                              fontFamily: AppTheme.font
                                          ),
                                        )),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(left: 7.0),
                                  child:
                                  SizedBox(
                                    height: 50.0,
                                    width: (MediaQuery.of(context).size.width / 2) - 30,
                                    child: ElevatedButton.icon(
                                        onPressed: () async {
                                        },
                                        label: const Text("Expense"),
                                        icon: FaIcon(FontAwesomeIcons.signOutAlt, size: 20, color:AppTheme.solidBtnText),
                                        style: ElevatedButton.styleFrom(
                                          primary: AppTheme.primary,
                                          onPrimary: AppTheme.solidBtnText,
                                          side: BorderSide(
                                              width: 1.0,
                                              color: AppTheme.primary
                                          ),
                                          textStyle: TextStyle(
                                              fontWeight: FontWeight.w800,
                                              color: AppTheme.primary,
                                              fontSize: 16,
                                              fontFamily: AppTheme.font
                                          ),
                                        )),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ],
                    )
                  ],
                )
              else if(_selectedIndex == 2)
                Text(
                  'Index 3: Home',
                  style: optionStyle,
                ),
            ],
          ),
        ),
        drawer: Theme(
            data: Theme.of(context).copyWith(
              canvasColor: utils.hexColor("#ffffff"),
            ),
            child: Drawer(
              child: Stack(children: <Widget>[
                ListView(
                  // Important: Remove any padding from the ListView.
                  padding: EdgeInsets.zero,
                  children: [
                    SizedBox(
                      height: 120.0,
                      child: GestureDetector(
                          child: DrawerHeader(
                              decoration: BoxDecoration(
                                  color: AppTheme.secondary,
                                  border: Border(
                                      bottom: BorderSide(
                                          color: utils.hexColor("#ffffff"),
                                          width: 5.0))),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 2,
                                    child: FittedBox(
                                      fit: BoxFit.contain,
                                      // otherwise the logo will be tiny
                                      child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius:
                                          BorderRadius.circular(30),
                                          border: Border.all(
                                            width: 2.0,
                                            color: utils.hexColor("#ffffff"),
                                          ),
                                        ),
                                        child: Wrap(
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                              BorderRadius.circular(25),
                                              child: CircleAvatar(
                                                  backgroundColor:
                                                  Colors.white30,
                                                  child: profileImgSet),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                      flex: 8,
                                      child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10.0, top: 5.0),
                                          child: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                userObj != null
                                                    ? utils.getCap(
                                                    "${userObj["first_name"]} ${userObj["last_name"]}")
                                                    : "-",
                                                textAlign: TextAlign.left,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: 18.0,fontFamily: AppTheme.font,
                                                    fontWeight: FontWeight.bold,
                                                    color: utils
                                                        .hexColor("#ffffff")),
                                              ),
                                              Text(
                                                  (userObj != null)
                                                      ? userObj["email"]
                                                      : "-",
                                                  textAlign: TextAlign.left,
                                                  overflow:
                                                  TextOverflow.ellipsis,
                                                  style: TextStyle(fontFamily: AppTheme.font,
                                                      color: utils.hexColor(
                                                          "#ffffff"))),
                                            ],
                                          )))
                                ],
                              )),
                          onTap: () {
                            Get.toNamed(AppPages.profile);
                          }),
                    ),
                    Container(
                        decoration: BoxDecoration(
                          color: utils.hexColor("#ffffff"),
                          border: Border(
                            bottom: BorderSide(
                                width: 1.0, color: utils.hexColor("#dddddd")),
                          ),
                        ),
                        child: ListTile(
                          title: Text(
                            'Dashboard',
                            style: TextStyle(fontFamily: AppTheme.font,
                                fontSize: 16.0,
                                color: utils.hexColor("#464646")),
                          ),
                          minLeadingWidth: 16,
                          leading: Icon(
                            FontAwesomeIcons.th,
                            color: AppTheme.primary,
                            size: 16.0,
                            semanticLabel: 'Dashboard',
                          ),
                          onTap: () {
                            Get.back();
                          },
                        )),
                    Container(
                        decoration: BoxDecoration(
                          color: utils.hexColor("#ffffff"),
                          border: Border(
                            bottom: BorderSide(
                                width: 1.0, color: utils.hexColor("#dddddd")),
                          ),
                        ),
                        child: ListTile(
                          title: Text('My Profile',
                              style: TextStyle(fontFamily: AppTheme.font,
                                fontSize: 16.0,
                                color: utils.hexColor("#464646"),
                              )),
                          minLeadingWidth: 16,
                          leading: Icon(
                            FontAwesomeIcons.userCircle,
                            color: AppTheme.primary,
                            size: 16.0,
                            semanticLabel: 'Profile',
                          ),
                          onTap: () {
                            Get.toNamed(AppPages.profile);
                          },
                        )),
                    Container(
                        decoration: BoxDecoration(
                          color: utils.hexColor("#ffffff"),
                          border: Border(
                            bottom: BorderSide(
                                width: 1.0, color: utils.hexColor("#dddddd")),
                          ),
                        ),
                        child: ListTile(
                          title: Text('Settings',
                              style: TextStyle(fontFamily: AppTheme.font,
                                fontSize: 16.0,
                                color: utils.hexColor("#464646"),
                              )),
                          minLeadingWidth: 16,
                          leading: Icon(
                            FontAwesomeIcons.cog,
                            color: AppTheme.primary,
                            size: 16.0,
                            semanticLabel: 'Profile',
                          ),
                          onTap: () {
                            Get.toNamed(AppPages.settings);
                          },
                        )),
                    Container(
                        decoration: BoxDecoration(
                          color: utils.hexColor("#ffffff"),
                          border: Border(
                            bottom: BorderSide(
                                width: 1.0, color: utils.hexColor("#dddddd")),
                          ),
                        ),
                        child: ListTile(
                          title: Text('Help & Support',
                              style: TextStyle(fontFamily: AppTheme.font,
                                fontSize: 16.0,
                                color: utils.hexColor("#464646"),
                              )),
                          minLeadingWidth: 16,
                          leading: Icon(
                            FontAwesomeIcons.lifeRing,
                            color: AppTheme.primary,
                            size: 16.0,
                            semanticLabel: 'Help & Support',
                          ),
                          onTap: () {
                            Get.toNamed(AppPages.helpAndSupport);
                          },
                        )),
                    Container(
                        decoration: BoxDecoration(
                          color: utils.hexColor("#ffffff"),
                          border: Border(
                            bottom: BorderSide(
                                width: 1.0, color: utils.hexColor("#dddddd")),
                          ),
                        ),
                        child: ListTile(
                          title: Text('Logout',
                              style: TextStyle(fontFamily: AppTheme.font,
                                fontSize: 16.0,
                                color: utils.hexColor("#464646"),
                              )),
                          minLeadingWidth: 16,
                          leading: Icon(
                            FontAwesomeIcons.signOutAlt,
                            color: AppTheme.primary,
                            size: 16.0,
                            semanticLabel: 'Logout',
                          ),
                          onTap: () async {
                            await authService.logout();
                            Get.offNamed(AppPages.login);
                          },
                        )),
                  ],
                ),
                Positioned(
                  left: 0.0,
                  bottom: 0.0,
                  child: Container(
                      width: MediaQuery.of(context).size.width - 85,
                      decoration: BoxDecoration(
                        color: utils.hexColor("#ffffff"),
                        border: Border(
                          bottom: BorderSide(
                              width: 1.0, color: utils.hexColor("#ffffff")),
                        ),
                      ),
                      child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Center(child:  Padding(
                              padding: const EdgeInsets.only(top: 30.0, bottom: 5.0),
                              child : Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Text('Powered by ',style: TextStyle(fontSize: 12, color: Colors.blueGrey),),
                                  Image.asset('assets/images/skyiots-logo.png',height: 20.0,),
                                  const Padding(
                                    padding: EdgeInsets.only(left: 5.0),
                                    child: Text('Skyiots Inc.',style: TextStyle(fontSize: 13, color: Colors.blueGrey, fontWeight: FontWeight.w800),),
                                  ),
                                ],
                              )),))),
                ),
              ]),
            )),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.book),
              label: 'My Ledger',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.add),
              label: 'Entry',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.menu),
              label: 'Menus',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: AppTheme.primary,
          backgroundColor: AppTheme.secondary,
          unselectedItemColor: AppTheme.disableColor,
          onTap: _onItemTapped,
        ),
      ),
    );
  }

  void getEntriesList() async{
    var res = await entryCtrl.getAllEntryList();
    if(res["status"]){
      setState(() {
        entryListArr = res["data"];
      });
    }
  }

  void getCounts() async{
    var res = await entryCtrl.getCounts();
    if(res["status"]){
      setState(() {
        totalIncome = res["result"]["income"];
        totalExpense = res["result"]["expense"];
        balanceAmount = res["result"]["balance"];
      });
    }
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget _headerOverview() {

    return Container(
      padding: const EdgeInsets.all(10.0),
      margin: const EdgeInsets.only(top: 10.0),
      height: 76,
      width: MediaQuery.of(context).size.width -20,
      decoration: BoxDecoration(
        color: utils.hexColor("#ffffff"),
        borderRadius: BorderRadius.circular(10.0),
        border: Border.all(color: utils.hexColor("#dddddd"), width: 1.0)
      ),
      child: Row(
        children: [
          Expanded(
              flex: 1,
              child: Padding(padding: EdgeInsets.all(5.0), child: Column(
            children: [
              Text("${balanceAmount}", style: TextStyle(fontWeight: FontWeight.w900, fontSize: 18.0, color: utils.hexColor("#363636")),),
              Padding(padding: EdgeInsets.only(top : 5), child: Text("Balance", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14.0, color: utils.hexColor("#666666")),),)
            ],
          ),)),
          Expanded(
              flex: 1,
              child: Padding(padding: EdgeInsets.all(5.0), child: Column(
            children: [
              Text("${totalExpense}", style: TextStyle(fontWeight: FontWeight.w900, fontSize: 18.0, color: utils.hexColor("#363636")),),
              Padding(padding: EdgeInsets.only(top : 5), child: Text("Expenses", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14.0, color: utils.hexColor("#666666")),),)
            ],
          ),)),
          Expanded(
              flex: 1,
              child: Padding(padding: EdgeInsets.all(5.0), child: Column(
            children: [
              Text("${totalIncome}", style: TextStyle(fontWeight: FontWeight.w900, fontSize: 18.0, color: utils.hexColor("#363636")),),
              Padding(padding: EdgeInsets.only(top : 5), child: Text("Incomes", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14.0, color: utils.hexColor("#666666")),),)
            ],
          ),))
        ],
      ),
    );
  }

  Widget _renderEntryWidgets() => Container(
    padding: const EdgeInsets.symmetric(horizontal: 4.0),
    margin: const EdgeInsets.only(top: 10.0),
    height: MediaQuery.of(context).size.height - 240,
    child: ListView.builder(
        padding: const EdgeInsets.all(0),
        scrollDirection: Axis.vertical,
        itemCount: entryListArr.length,
        itemBuilder: (b, i) {
          var oneItem = entryListArr[i];
          return _singleEntryCard(b, i, oneItem);
        }),
  );

  Widget _singleEntryCard(b, i, oneItem) {

    var entryColor = "";
    var categoryLabelColor = "";

    if(oneItem["entry_type"] == "expense"){
      entryColor = "#d91e18"; //red - expense
      categoryLabelColor = "#ffe4e4"; //red - bg
    }else{
      entryColor = "#26c281"; //green - income
      categoryLabelColor = "#cdf4e2"; //green - bg
    }

    return Container(
      padding: const EdgeInsets.all(12.0),
      margin: const EdgeInsets.all(5.0),
      decoration: BoxDecoration(
          color: utils.hexColor("#ffffff"),
          border: Border.all(
              width: 1.0,
              color: utils.hexColor("#dddddd")
          ),
          borderRadius: BorderRadius.circular(10.0)
      ),
      child: Column(
        children: [
          Row(
            children: [
              Icon(FontAwesomeIcons.dotCircle,color: utils.hexColor("#777777"), size:16),
              Padding(padding: EdgeInsets.only(left: 5.0),
                child: SizedBox(
                    width: MediaQuery.of(context).size.width - 180,
                    child: Text("${utils.dhzCapitalize(oneItem['expense_details'])}",
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 14.0,fontWeight: FontWeight.w800, overflow: TextOverflow.ellipsis, color: utils.hexColor("#363636")),
                    )
                ),),
              Expanded(
                child: Row(),
              ),
              Icon(FontAwesomeIcons.rupeeSign, color: utils.hexColor(entryColor), size:14),
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Text("${oneItem['amount']}", style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w800, color: utils.hexColor(entryColor)),textAlign: TextAlign.right,),
              )
            ],
          ),
          Padding(padding: const EdgeInsets.only(top: 8.0), child: Row(
            children: [
              Container(
                padding: const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
                decoration: BoxDecoration(
                  color: utils.hexColor(categoryLabelColor),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Text("${utils.dhzCapitalize(oneItem['entry_type'])}", style: TextStyle(fontSize: 13.0, color: utils.hexColor(entryColor)),),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
                margin: EdgeInsets.only(left: 7.0),
                decoration: BoxDecoration(
                  color: utils.hexColor("#eeeeee"),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Text("${utils.dhzCapitalize(oneItem['category'])}", style: TextStyle(fontSize: 13.0,color: utils.hexColor("#464646")),),
              ),
              Expanded(
                child: Row(),
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Text("${oneItem['paid_date']} ${oneItem['paid_time']}", style: TextStyle(fontSize: 14.0,color: utils.hexColor("#787878")),textAlign: TextAlign.right,),
              )
            ],
          ),)
        ],
      ),
    );
  }

  void getUserProfileImg() async {
    setState(() {
      Env.conf["user_pro_img_id"] = userObj["hphoto"];
      profileImgSet = Image.network(
        '${Env.conf["api_base_path"]}/files/download/${Env.conf["api_token"]}/${Env.conf["user_pro_img_id"]}?${DateTime.now().millisecondsSinceEpoch}',
        height: 60.0,
        width: 60.0,
        fit: BoxFit.cover,
      );
    });
  }
  
  void pageInit() async{
    getUserDetails();
  }

  Future<void> getUserDetails() async{
    var user = await authService.getUserObj();
    print("user------------------");
    print(user);
    setState(() {
      userObj = user;
    });
  }

  @override
  void initState() {
    super.initState();
    pageInit();
    getEntriesList();
    getCounts();

  }
}