import 'package:flutter/services.dart';
import 'package:ems_mob_app/routes/app_pages.dart';
import 'package:ems_mob_app/services/auth_api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:ems_mob_app/controllers/login_controller.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:ems_mob_app/modules/utils/constant_variables.dart';
import 'package:get/get.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../../app-conf.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool isChecked = false;
  bool onchange = false;
  bool reset = false;

  LoginController loginCtrl = LoginController();
  AuthApiService authApiService = AuthApiService();

  TextEditingController emailCtrl =  TextEditingController();
  TextEditingController passwordCtrl =  TextEditingController();
  TextEditingController primaryPhoneCtrl =  TextEditingController();
  TextEditingController firstNameCtrl =  TextEditingController();
  TextEditingController lastNameCtrl =  TextEditingController();
  // TextEditingController countryCtrl =  TextEditingController();
  // TextEditingController stateCtrl =  TextEditingController();
  // TextEditingController cityCtrl =  TextEditingController();
  // TextEditingController addressCtrl =  TextEditingController();
  // TextEditingController zipcodeCtrl =  TextEditingController();
  // TextEditingController localeCtrl =  TextEditingController();
  // TextEditingController timezoneCtrl =  TextEditingController();

  bool _signupBtnEnabled = true;
  String _loginBtnTxt = "Confirm, Proceed";
  UtilService utils = UtilService();

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      return AppTheme.outLineBorder;
    }
    return Scaffold(
      body: Center(
          child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: EdgeInsets.all(30.0),
                  child: Column(
                    children: [
                      Padding(
                          padding: EdgeInsets.only(top: 0.0, bottom: 1.0),
                          child: Image.asset('assets/images/logo.png',height: 90.0,fit: BoxFit.cover,)
                      ),
                      Padding(
                          padding: EdgeInsets.only(top: 8.0, bottom: 10.0),
                          child: Row(
                            mainAxisAlignment : MainAxisAlignment.center,
                            children: [
                              Text.rich(
                                TextSpan(
                                  text: 'Yes',
                                  style: TextStyle(fontSize: 21, color: AppTheme.secondary, fontWeight: FontWeight.w800, fontFamily: AppTheme.font),
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  text: 'Ledgers',
                                  style: TextStyle(fontSize: 21, color: AppTheme.primaryDark, fontWeight: FontWeight.w800, fontFamily: AppTheme.font),
                                ),
                              )
                            ],
                          )
                      ),
                      Padding(
                          padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                          child: Text.rich(
                            TextSpan(
                              text: 'Sign Up',
                              style: TextStyle(fontSize: 20, color: utils.hexColor("#363636"), fontFamily: AppTheme.font),
                            ),
                          )),
                      Padding(
                          padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                          child: SizedBox(
                            // height: 50.0,
                              width: MediaQuery.of(context).size.width - 30,
                              child: TextFormField(
                                  controller: firstNameCtrl,
                                  validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Enter Your First Name';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'First Name',
                                    // contentPadding: EdgeInsets.all(20.0),
                                    hintText: '****',
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                    ),
                                  ),
                                  onChanged: (val) {
                                    if(onchange){
                                      if (_formKey.currentState!.validate()) {}
                                    }
                                  }))),
                      Padding(
                          padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                          child: SizedBox(
                            // height: 50.0,
                              width: MediaQuery.of(context).size.width - 30,
                              child: TextFormField(
                                  controller: lastNameCtrl,
                                  validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Enter Your First Name';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Last Name',
                                    // contentPadding: EdgeInsets.all(20.0),
                                    hintText: '****',
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                    ),
                                  ),
                                  onChanged: (val) {
                                    if(onchange){
                                      if (_formKey.currentState!.validate()) {}
                                    }
                                  }))),
                      Padding(
                          padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                          child: SizedBox(
                            // height: 50.0,
                              width: MediaQuery.of(context).size.width - 30,
                              child: TextFormField(
                                  controller: emailCtrl,
                                  obscureText: false,
                                  validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Enter you email id';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Email ID',
                                    // contentPadding: EdgeInsets.all(20.0),
                                    hintText: '****',
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                    ),
                                  ),
                                  onChanged: (val) {
                                    if(onchange){
                                      if (_formKey.currentState!.validate()) {}
                                    }
                                  }))),
                      Padding(
                          padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                          child: SizedBox(
                              width: MediaQuery.of(context).size.width - 30,
                              child: TextFormField(
                                  controller: passwordCtrl,
                                  obscureText: true,
                                  validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Enter you password';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Password',
                                    // contentPadding: EdgeInsets.all(20.0),
                                    hintText: '****',
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                    ),
                                  ),
                                  onChanged: (val) {
                                    if(onchange){
                                      if (_formKey.currentState!.validate()) {}
                                    }
                                  }))),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                        child: SizedBox(
                          height: 50.0,
                          width: MediaQuery.of(context).size.width,
                          child: ElevatedButton.icon(
                              onPressed: _signupBtnEnabled ? (){
                                if(firstNameCtrl.text == ""){
                                  utils.fToast("First Name is required","bottom");
                                }
                                else if(lastNameCtrl.text == ""){
                                  utils.fToast("Last Name is required","bottom");
                                }
                                else if(emailCtrl.text == ""){
                                  utils.fToast("Email ID is required","bottom");
                                }
                                else if(passwordCtrl.text == ""){
                                  utils.fToast("Password is required","bottom");
                                }

                                onchange = true;
                                if (_formKey.currentState!.validate()) {

                                  if(_signupBtnEnabled){
                                    setState(() {
                                      _signupBtnEnabled  = false;
                                      _loginBtnTxt  = "Loading...";
                                    });

                                    EasyLoading.show(status: 'loading...',maskType: EasyLoadingMaskType.black);

                                    var userObj = {
                                      "firstname": firstNameCtrl.text,
                                      "lastname": lastNameCtrl.text,
                                      "email": emailCtrl.text,
                                      "password": passwordCtrl.text
                                    };

                                    print("userObj---------------1");
                                    print(userObj);

                                    loginCtrl.userSignUp(userObj).then((result) async {
                                      print("userObj---------------2");
                                      print(userObj);
                                      EasyLoading.dismiss();

                                      setState(() {
                                        _signupBtnEnabled = true;
                                        _loginBtnTxt = "Confirm, Proceed";
                                      });

                                      if (result["status"]) {
                                        utils.fToast("Registration Completed Successfully!", "bottom");
                                        Get.offNamed(AppPages.login);
                                      } else {
                                        utils.fToast("${result["data"]}","bottom");
                                      }
                                    });
                                  }
                                }
                              } : null,
                              label: Text(_loginBtnTxt),
                              icon: FaIcon(FontAwesomeIcons.check, size: 18, color:AppTheme.solidBtnText),
                              style: ElevatedButton.styleFrom(
                                primary: AppTheme.primary,
                                textStyle: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w800,
                                  fontSize: 16,
                                ),
                              )),
                        ),
                      ),
                      Padding(
                          padding: const EdgeInsets.only(top: 30.0, bottom: 5.0),
                          child : Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const Text('Powered by ',style: TextStyle(fontSize: 12, color: Colors.blueGrey),),
                              Image.asset('assets/images/skyiots-logo.png',height: 20.0,),
                              const Padding(
                                padding: EdgeInsets.only(left: 5.0),
                                child: Text('Skyiots Inc.',style: TextStyle(fontSize: 13, color: Colors.blueGrey, fontWeight: FontWeight.w800),),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              )
          )),
    );
  }

  @override
  void initState(){

    firstNameCtrl.text = "Jhon";
    lastNameCtrl.text = "Doe";
    emailCtrl.text = "jhondoe@yesledgers.com";
    passwordCtrl.text = "welcome123";
  }
}
