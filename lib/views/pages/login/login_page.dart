import 'package:ems_mob_app/routes/app_pages.dart';
import 'package:ems_mob_app/services/auth_api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:ems_mob_app/controllers/login_controller.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:ems_mob_app/modules/utils/constant_variables.dart';
import 'package:get/get.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

var APP_VERSION = "Version 1.0.0";

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool isChecked = false;
  bool onchange = false;

  LoginController loginCtrl = LoginController();
  AuthApiService authApiService = AuthApiService();

  TextEditingController emailCtrl = new TextEditingController();
  TextEditingController pwdCtrl = new TextEditingController();

  bool isEmailValidated = false;
  bool _loginBtnEnabled = true;
  String _loginBtnTxt = "Login";
  UtilService utils = UtilService();

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      return AppTheme.primary;
    }
    return Scaffold(
      body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height - 390,
                  decoration: BoxDecoration(
                    color: AppTheme.fadeSecondary,
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(60.0),
                      bottomRight: Radius.circular(60.0),
                    ),
                  ),
                  child: Padding(
                      padding: EdgeInsets.only(top: 70.0, left: 40.0, bottom: 1.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 70.0, bottom: 1.0),
                              child: Image.asset('assets/images/logo.png',height: 70.0,fit: BoxFit.cover,)
                          ),
                          Padding(
                              padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                              child: Text.rich(
                                TextSpan(
                                  text: 'Welcome back to',
                                  style: TextStyle(fontSize: 18, color: AppTheme.darkColor, fontWeight: FontWeight.w300),
                                ),
                              )),
                          Padding(
                              padding: EdgeInsets.only(top: 8.0, bottom: 10.0),
                              child: Row(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      text: 'Yes',
                                      style: TextStyle(fontSize: 28, color: AppTheme.secondary, fontWeight: FontWeight.w800, fontFamily: AppTheme.font),
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      text: 'Ledgers',
                                      style: TextStyle(fontSize: 28, color: AppTheme.primaryDark, fontWeight: FontWeight.w800, fontFamily: AppTheme.font),
                                    ),
                                  )
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                              child: Text.rich(
                                TextSpan(
                                  text: 'Sign In',
                                  style: TextStyle(fontSize: 27, color: AppTheme.darkColor, fontWeight: FontWeight.w800, fontFamily: AppTheme.font),
                                ),
                              )),

                        ],
                      )
                  )
              ),
              Wrap(
                children: [
                  Center(
                    child:  Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          Padding(
                              padding: EdgeInsets.all(7.0),
                              child: SizedBox(
                                // height: 50.0,
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: TextFormField(
                                      textAlignVertical: TextAlignVertical.center,
                                      textAlign: TextAlign.center,
                                      controller: emailCtrl,
                                      style: TextStyle(color: AppTheme.darkColor, fontSize: 16.0),
                                      obscureText: false,
                                      validator: (String? value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Email ID is required';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        // contentPadding: EdgeInsets.all(20.0),
                                        hintText: 'Email ID',
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                      ),
                                      onChanged: (val) {
                                        if(onchange){
                                          if (_formKey.currentState!.validate()) {
                                            if(!emailCtrl.text.isEmpty){

                                            }
                                          }
                                        }
                                      }))),
                          Padding(
                              padding: EdgeInsets.all(7.0),
                              child: SizedBox(
                                // height: 50.0,
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: TextFormField(
                                      textAlignVertical: TextAlignVertical.center,
                                      textAlign: TextAlign.center,
                                      controller: pwdCtrl,
                                      style: TextStyle(color: AppTheme.darkColor, fontSize: 16.0),
                                      obscureText: true,
                                      validator: (String? value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Password is required!';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        hintText: 'Password',
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(color: utils.hexColor("#cccccc")),
                                        ),
                                      ),
                                      onChanged: (val) {
                                        if(onchange){
                                          if (_formKey.currentState!.validate()) {
                                            if(pwdCtrl.text.isEmpty){

                                            }
                                          }
                                        }
                                      }))),
                          Padding(
                            padding: EdgeInsets.only(right: 15,top :5.0, bottom: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                GestureDetector(
                                  onTap: (){
                                    Get.toNamed(AppPages.forgotPassword);
                                  },
                                  child: Text("Forgot Password ?",style: TextStyle(
                                      decoration: TextDecoration.underline,
                                      fontSize: 16.0
                                  ),),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: 50.0,
                                  width: MediaQuery.of(context).size.width/2,
                                  child: ElevatedButton.icon(
                                      onPressed: _loginBtnEnabled ? () async {
                                        onchange = true;
                                        if (_formKey.currentState!.validate()) {
                                          if(_loginBtnEnabled){
                                            setState(() {
                                              _loginBtnEnabled  = false;
                                              _loginBtnTxt  = "Loading...";
                                            });

                                            var inputObj = {
                                              "username" : emailCtrl.text,
                                              "password" : pwdCtrl.text
                                            };
                                            var res = await loginCtrl.userLogin(inputObj);
                                            if(res["status"]){
                                              EasyLoading.show(status: 'loading...',maskType: EasyLoadingMaskType.black);
                                              EasyLoading.dismiss();
                                              Get.offNamed(AppPages.dashboard);

                                              setState(() {
                                                _loginBtnEnabled = true;
                                                _loginBtnTxt = "Login";
                                              });
                                            }else{
                                              utils.fToast("Enter a valid PIN", "bottom");
                                            }
                                          }
                                        }
                                      } : null,
                                      label: Text(_loginBtnTxt),
                                      icon: const Icon(Icons.arrow_forward),
                                      style: ElevatedButton.styleFrom(
                                        primary: AppTheme.primary,
                                        textStyle: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w800,
                                            fontFamily: AppTheme.font
                                        ),
                                      )),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: 50.0,
                                  width: MediaQuery.of(context).size.width/2,
                                  child: ElevatedButton.icon(
                                      onPressed: () async {
                                        Get.toNamed(AppPages.signup);
                                      },
                                      label: const Text("Sign Up"),
                                      icon: FaIcon(FontAwesomeIcons.signOutAlt, size: 20, color:AppTheme.secondary),
                                      style: ElevatedButton.styleFrom(
                                        primary: AppTheme.outLineBg,
                                        onPrimary: AppTheme.secondary,
                                        side: BorderSide(
                                            width: 1.0,
                                            color: AppTheme.secondary
                                        ),
                                        textStyle: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            color: AppTheme.secondary,
                                            fontSize: 16,
                                            fontFamily: AppTheme.font
                                        ),
                                      )),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(top: 30.0, bottom: 5.0),
                              child : Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Text('Powered by ',style: TextStyle(fontSize: 12, color: Colors.blueGrey),),
                                  Image.asset('assets/images/skyiots-logo.png',height: 20.0,),
                                  const Padding(
                                    padding: EdgeInsets.only(left: 5.0),
                                    child: Text('Skyiots Inc.',style: TextStyle(fontSize: 13, color: Colors.blueGrey, fontWeight: FontWeight.w800),),
                                  ),
                                ],
                              )),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
      )
    );
  }

  @override
  void initState(){
    emailCtrl.text = "saravanans.design@gmail.com";
    pwdCtrl.text = "welcome123";
  }
}
