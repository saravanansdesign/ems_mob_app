import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:ems_mob_app/modules/utils/constant_variables.dart';
import 'package:ems_mob_app/services/auth_api.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ems_mob_app/routes/app_pages.dart';
import 'package:get/get.dart';

class HelpAndSupportPage extends StatefulWidget {
  const HelpAndSupportPage({Key? key}) : super(key: key);

  @override
  _HelpAndSupportPageState createState() => _HelpAndSupportPageState();
}

class _HelpAndSupportPageState extends State<HelpAndSupportPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  AuthApiService authService = AuthApiService();
  UtilService utils = UtilService();
  Map? userObj;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: const Text('Help & Support',
          style: TextStyle(color: Colors.white)),
          backgroundColor: AppTheme.primary,
      ),
      body:SingleChildScrollView(

        child: Container(
          margin: EdgeInsets.symmetric(vertical:15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 50,left: 25,right: 25),
                    // padding: EdgeInsets.symmetric(horizontal: 12,vertical:30),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        width: 1.0,
                        color: utils.hexColor("#dddddd"),
                      ),
                    ),
                    child: Column(
                      children: [
                        GestureDetector(
                          child: Padding(
                            padding: EdgeInsets.only(top: 30, bottom: 5, right: 15, left: 15),
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text("For Support Contact", style: TextStyle(fontSize: 16, color: utils.hexColor("#363636")))),
                                FaIcon(FontAwesomeIcons.chevronRight,size:20,color:AppTheme.primary)
                              ],
                            ),
                          ),
                          onTap: (){
                            utils.openBrowserTab("https://boodskap.io/contact-us");
                          },
                        ),
                        Divider(
                          thickness: 1.0,
                          color: utils.hexColor("#dddddd"),
                        ),

                        GestureDetector(
                          child: Padding(
                            padding: EdgeInsets.only(top: 5, bottom: 5, right: 15, left: 15),
                            child: Row(
                              children: [
                                Expanded(child: Text("Privacy Policy", style: TextStyle(fontSize: 16, color: utils.hexColor("#363636")))),
                                FaIcon(FontAwesomeIcons.chevronRight,size:20,color:AppTheme.primary),
                              ],
                            ),
                          ),
                          onTap: (){
                            utils.openBrowserTab("https://boodskap.io/privacy-policy");
                          },
                        ),
                        Divider(
                          thickness: 1.0,
                          color: utils.hexColor("#dddddd"),
                        ),

                        GestureDetector(
                          child: Padding(
                            padding: EdgeInsets.only(top: 5, bottom: 5, right: 15, left: 15),
                            child: Row(
                              children: [
                                Expanded(child: Text("Terms & Conditions", style: TextStyle(fontSize: 16, color: utils.hexColor("#363636")))),
                                FaIcon(FontAwesomeIcons.chevronRight,size:20,color:AppTheme.primary),
                              ],
                            ),
                          ),
                          onTap: (){
                            utils.openBrowserTab("https://boodskap.io/terms-condition");
                          },
                        ),
                        Divider(
                          thickness: 1.0,
                          color: utils.hexColor("#dddddd"),
                        ),

                        GestureDetector(
                          child: Padding(
                            padding: EdgeInsets.only(top: 5, bottom: 5, right: 15, left: 15),
                            child: Row(
                              children: [
                                Expanded(child: Text("EULA", style: TextStyle(fontSize: 16, color: utils.hexColor("#363636")))),
                                FaIcon(FontAwesomeIcons.chevronRight,size:20,color:AppTheme.primary),
                              ],
                            ),
                          ),
                          onTap: (){
                            utils.openBrowserTab("https://boodskap.io/eula");
                          },
                        ),
                        Divider(
                          thickness: 1.0,
                          color: utils.hexColor("#dddddd"),
                        ),

                        GestureDetector(
                          child: Padding(
                            padding: EdgeInsets.only(top: 5, bottom: 5, right: 15, left: 15),
                            child: Row(
                              children: [
                                Expanded(child: Text("About Us", style: TextStyle(fontSize: 16, color: utils.hexColor("#363636")))),
                                FaIcon(FontAwesomeIcons.chevronRight,size:20,color:AppTheme.primary),
                              ],
                            ),
                          ),
                          onTap: (){
                            utils.openBrowserTab("https://boodskap.io/company");
                          },
                        ),
                        Divider(
                          thickness: 1.0,
                          color: utils.hexColor("#dddddd"),
                        ),

                        GestureDetector(
                          child: Padding(
                            padding: EdgeInsets.only(top: 5, bottom: 15, right: 15, left: 15),
                            child: Row(
                              children: [
                                Expanded(child: Text("App Info", style: TextStyle(fontSize: 16, color: utils.hexColor("#363636")))),
                                FaIcon(FontAwesomeIcons.chevronRight,size:20,color:AppTheme.primary),
                              ],
                            ),
                          ),
                          onTap: (){
                            Get.toNamed(AppPages.appInfo);
                          },
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: 25,
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          boxShadow: [BoxShadow( offset: Offset(0,5),blurRadius: 10,spreadRadius: 1, color: AppTheme.primary)],
                        ),
                        child: CircleAvatar(
                          radius: 25,
                          child: ClipOval(child:Icon(FontAwesomeIcons.questionCircle,size:20,color:Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }
}
