import 'package:flutter/material.dart';
import 'package:ems_mob_app/controllers/dashboard_controller.dart';
import 'package:ems_mob_app/controllers/schedules_controller.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ems_mob_app/modules/utils/constant_variables.dart';
import 'package:ems_mob_app/routes/app_pages.dart';
import 'package:ems_mob_app/controllers/settings_controller.dart';
import 'package:get/get.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:ems_mob_app/services/auth_api.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  Map<String, bool> notificationObj = {
    "pushToggle" : false,
    "mailToggle" : false,
    "smsToggle" : false,
  };

  UtilService utils = UtilService();
  SettingsController settingsCtrl = SettingsController();
  AuthApiService authApi  = AuthApiService();
  Map? userObj;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Settings',
          style: TextStyle(fontWeight: FontWeight.bold,fontFamily: AppTheme.font,color: Colors.white)),
          backgroundColor: AppTheme.primary,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(vertical:15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 50,left: 25,right: 25),
                    padding: EdgeInsets.symmetric(horizontal: 12,vertical:30),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        width: 1.0,
                        color: utils.hexColor("#dddddd"),
                      ),
                    ),
                    // height: 100,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Text("Push Notifications",style: TextStyle(fontSize: 16, color: utils.hexColor("#363636")))),

                              ////////////SWITCH////////////////////
                              FlutterSwitch(
                                width: 50.0,
                                height: 25.0,
                                toggleSize: 20.0,
                                value: notificationObj["pushToggle"] as bool,
                                borderRadius: 30.0,
                                activeToggleColor: Colors.white,
                                inactiveToggleColor: Colors.white,
                                activeColor: AppTheme.primary,
                                inactiveColor: AppTheme.primary,
                                activeIcon: Icon(FontAwesomeIcons.phoneAlt,size:20,color:AppTheme.primary),
                                inactiveIcon: Icon(FontAwesomeIcons.phoneAlt,size:20,color:AppTheme.primary),
                                onToggle: (val) async {
                                  setState(() {
                                    notificationObj["pushToggle"] = val;
                                  });
                                  updateHostNotification();
                                },
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 8),
                          child: Divider(
                            thickness: 1.0,
                            color: utils.hexColor("#dddddd"),
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text("Email Notifications",style: TextStyle(fontSize: 16, color: utils.hexColor("#363636")))),

                            //////////////////////////SWITCH/////////////////////////////////
                            FlutterSwitch(
                              width: 50.0,
                              height: 25.0,
                              toggleSize: 20.0,
                              value: notificationObj["mailToggle"] as bool,
                              borderRadius: 30.0,
                              activeToggleColor: Colors.white,
                              inactiveToggleColor: Colors.white,
                              activeColor: AppTheme.primary,
                              inactiveColor: AppTheme.primary,
                              activeIcon: Icon(FontAwesomeIcons.envelope,size:20,color:AppTheme.primary),
                              inactiveIcon: Icon(FontAwesomeIcons.envelope,size:20,color:AppTheme.primary),
                              onToggle: (val) {
                                setState(() {
                                  notificationObj["mailToggle"] = val;
                                });
                                updateHostNotification();
                              },
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 8),
                          child: Divider(
                            thickness: 1.0,
                            color: utils.hexColor("#dddddd"),
                          ),
                        ),
                        Row(
                          children: [
                            Expanded( child: Text("SMS Notifications",style: TextStyle(fontSize: 16, color: utils.hexColor("#363636")))),
                            FlutterSwitch(
                              width: 50.0,
                              height: 25.0,
                              toggleSize: 20.0,
                              value: notificationObj["smsToggle"] as bool,
                              borderRadius: 30.0,
                              activeToggleColor: Colors.white,
                              inactiveToggleColor: Colors.white,
                              activeColor: AppTheme.primary,
                              inactiveColor: AppTheme.primary,
                              activeIcon: Icon(FontAwesomeIcons.envelopeOpenText,size:20,color:AppTheme.primary),
                              inactiveIcon: Icon(FontAwesomeIcons.envelopeOpenText,size:20,color:AppTheme.primary),
                              onToggle: (val) {
                                setState(() {
                                  notificationObj["smsToggle"] = val;
                                });
                                updateHostNotification();
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: 25,
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          boxShadow: [BoxShadow( offset: Offset(0,5),blurRadius: 10,spreadRadius: 1, color: utils.hexColor("#aaaaaa"))],
                        ),
                        child: CircleAvatar(
                          backgroundColor : AppTheme.secondary,
                          radius: 25,
                          child: ClipOval(
                            child:Icon(FontAwesomeIcons.bell,size:20,color:Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),


              Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 50,left: 25,right: 25),
                    padding: EdgeInsets.symmetric(horizontal: 12,vertical:30),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        width: 1.0,
                        color: utils.hexColor("#dddddd"),
                      ),
                    ),
                    child: Column(
                      children: [
                        /*Padding(
                          padding: EdgeInsets.only(top: 15),
                          child: GestureDetector(
                            onTap: (){
                              Get.toNamed(AppPages.preferences);
                            },
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text("Preferences", style: TextStyle(fontSize: 16, color: utils.hexColor("#363636")))),
                                FaIcon(FontAwesomeIcons.slidersH,size:20,color:AppTheme.primary)
                              ],
                            ),
                          ),
                        ),*/
                        /* Padding(
                          padding: EdgeInsets.symmetric(vertical: 8),
                          child: Divider(
                            thickness: 1.0,
                            color: utils.hexColor("#dddddd"),
                          ),
                        ),*/
                        GestureDetector(
                          onTap: (){
                            Get.toNamed(AppPages.changePassword);
                          },
                          child: Row(
                            children: [
                                  Expanded(child: Text("Change Password", style: TextStyle(fontSize: 16, color: utils.hexColor("#363636")))),
                                   FaIcon(FontAwesomeIcons.key,size:20,color:AppTheme.primary),
                              ],
                          ),
                        ),
                        /*Padding(
                          padding: EdgeInsets.symmetric(vertical: 8),
                          child: Divider(
                            thickness: 1.0,
                            color: utils.hexColor("#dddddd"),
                          ),
                        ),
                        GestureDetector(
                          onTap: (){
                            Get.toNamed(AppPages.preferences);
                          },
                          child:Row(
                          children: [
                            Expanded(child: Text("Date & Time Format",style: TextStyle(fontSize: 16, color: utils.hexColor("#363636")))),
                            FaIcon(FontAwesomeIcons.clock,size:20,color:AppTheme.primary),
                          ],
                        ),
                        ),*/
                      ],
                    ),
                  ),
                  Positioned(
                    top: 25,
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          boxShadow: [BoxShadow( offset: Offset(0,5),blurRadius: 10,spreadRadius: 1, color: utils.hexColor("#aaaaaa"))],
                        ),
                        child: CircleAvatar(
                          backgroundColor : AppTheme.secondary,
                          radius: 25,
                          child: ClipOval(child:Icon(FontAwesomeIcons.cog,size:20,color:Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> updateHostNotification() async {
    EasyLoading.show(status: 'loading...',maskType: EasyLoadingMaskType.black);
    var res = await settingsCtrl.updateHostNotificationCtrl(notificationObj);
    EasyLoading.dismiss();
    if(res["status"]){
      settingsCtrl.getCustomerDetails();
      utils.fToast("Push Notification Settings Updated Successfully!", "bottom");
      // utils.sweetAlert("success", context, "Success", "Push Notification Settings Updated Successfully!");
    }else{
      utils.fToast("Oops! Can not update the settings, Please Try again later", "bottom");
      // utils.sweetAlert("error", context, "Failed", "Oops! Can not update the settings, Please Try again later");
    }
  }

  Future<void> getNotificationDetails() async {
    var customerObj;
    /*var customerObj = await authApi.getCustomerObj();
    setState(() {
      notificationObj = {
        "pushToggle" : customerObj["is_push_enabled"],
        "mailToggle" : customerObj["is_email_enabled"],
        "smsToggle" : customerObj["is_sms_enabled"]
      };
    });*/
    EasyLoading.show(status: 'loading...',maskType: EasyLoadingMaskType.black);
    var res = await settingsCtrl.getCustomerDetails();
    EasyLoading.dismiss();
    if(res["status"]){
      customerObj = res["result"]["data"]["data"][0];
      setState(() {
        notificationObj = {
          "pushToggle" : customerObj["is_push_enabled"],
          "mailToggle" : customerObj["is_email_enabled"],
          "smsToggle" : customerObj["is_sms_enabled"]
        };
      });
    }else{
      utils.fToast("Can not fetch the notification configuration", "bottom");
    }
  }

  @override
  void initState() {
    getNotificationDetails();
  }
}
