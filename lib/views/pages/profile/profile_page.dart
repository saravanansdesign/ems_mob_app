import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ems_mob_app/controllers/dashboard_controller.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:ems_mob_app/modules/utils/constant_variables.dart';
import 'package:ems_mob_app/routes/app_pages.dart';
import 'package:ems_mob_app/services/auth_api.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'package:image_picker/image_picker.dart';
import 'package:ems_mob_app/app-conf.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  UtilService utils = UtilService();
  AuthApiService authService = AuthApiService();
  DashboardController dashCtrl = DashboardController();

  var inputObj = {};
  var userObj;
  var customerObj;
  String isProfilePic = "default";

  var profileImgSet = Image.asset(
    'assets/images/user.png',
    fit: BoxFit.cover,
  );

  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController emailAddress = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController empId = TextEditingController();
  TextEditingController department = TextEditingController();
  AuthApiService authApi = AuthApiService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: const Text('My Profile', style: TextStyle(color: Colors.white)),
        backgroundColor: AppTheme.primary,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 25.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Stack(
                    children: <Widget>[
                      /*CircleAvatar(
                        radius: 50,
                        child: ClipOval(
                          child: Image.asset(
                            'assets/images/boodskap-logo.png',
                            scale: 3,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),*/

                      GestureDetector(
                        onTap: () {

                        },
                        child: Container(
                          width: 90,
                          height: 90,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            border: Border.all(
                              width: 3,
                              color: AppTheme.primary,
                            ),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: CircleAvatar(
                                backgroundColor: Colors.white30,
                                child: profileImgSet),
                          ),
                        ),
                      ),
                      Positioned(
                          bottom: 1,
                          right: 1,
                          child: Container(
                            height: 20,
                            width: 20,
                            child: Icon(
                              FontAwesomeIcons.pencilAlt,
                              color: AppTheme.primary,
                              size: 10,
                            ),
                            decoration: BoxDecoration(
                                color: utils.hexColor("#ffffff"),
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(
                                  width: 1,
                                  color: AppTheme.primary,
                                ),),
                          ))
                    ],
                  ),
                ),
              ),
              Padding(
                padding:  EdgeInsets.symmetric(vertical: 5),
                child: Center(
                    child: Text("${utils.isNull(userObj) ? userObj["user"]["firstName"] : "-"} ${utils.isNull(userObj) ? userObj["user"]["lastName"] : "-"}", style: TextStyle(fontWeight: FontWeight.bold,fontFamily: AppTheme.font,fontSize: 18.0),)),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20.0),
                child: Center(child: Text("${utils.isNull(customerObj) ? customerObj["hemail"] : "-"}",style: TextStyle(fontSize: 15.0))),
              ),
              Form(
                key: _formKey,
                child: Container(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                    color: utils.hexColor("#ffffff"),
                    border: Border.all(
                      width: 1,
                      color: utils.hexColor("#dddddd"),
                    ),
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: [
                      BoxShadow(
                        color: utils.hexColor("#dddddd"),
                        offset: Offset(0.0,3.0),
                      ),
                    ],
                  ),
                  child: Column(
                    children: [
                      Padding(
                          padding: EdgeInsets.only(top: 15),
                          child: SizedBox(
                              width: MediaQuery.of(context).size.width - 30,
                              child: TextFormField(
                                controller: firstName,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'First Name',
                                  contentPadding: EdgeInsets.only(
                                      left: 15, bottom: 11, top: 11, right: 15),
                                ),
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Enter your first name';
                                  }
                                  return null;
                                },
                              ))),
                      Padding(
                          padding: EdgeInsets.only(top: 15),
                          child: SizedBox(
                            // height: 50.0,
                              width: MediaQuery.of(context).size.width - 30,
                              child: TextFormField(
                                  controller: lastName,
                                  validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Enter your last name';
                                    }
                                    return null;
                                  },
                                  decoration: const InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'Last Name',
                                    contentPadding: EdgeInsets.only(
                                        left: 15, bottom: 11, top: 11, right: 15),
                                  ),
                                  onChanged: (val) {
                                    // print(val);
                                  }))),
                      Padding(
                        padding: EdgeInsets.only(top: 20.0),
                        child: MaterialButton(
                            height: 50.0,
                            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                            elevation: 20,
                            minWidth: double.infinity,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(right: 10.0),
                                  child: Icon(FontAwesomeIcons.check,  color: utils.hexColor("#ffffff"),size: 16,),
                                ),
                                Text( "Update Profile", style: TextStyle(color: Colors.white, fontSize: 16.0),),
                              ],
                            ),
                            color: AppTheme.primary,
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                var profileObj = {};
                                profileObj["firstName"] = firstName.text;
                                profileObj["lastName"] = lastName.text;
                                profileObj["hdept"] = department.text;
                              }
                            }),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void getUserProfileImg() async {
    // var res = await authService.getUserProperty(userObj!["user"]["email"]);
    // if (res["status"]) {
    setState(() {
      Env.conf["user_pro_img_id"] = utils.isNull(customerObj) ? customerObj["hphoto"] : "-";
      profileImgSet = Image.network(
        '${Env.conf["api_base_path"]}/files/download/${Env.conf["api_token"]}/${Env.conf["user_pro_img_id"]}?${DateTime.now().millisecondsSinceEpoch}',
        fit: BoxFit.cover,
      );
    });
    // }
  }

  Future<void> getUserDetails() async{
    var user = await authService.getUserObj();
    setState(() {
      userObj = user;
    });

    if(utils.isNull(userObj)){
      firstName.text = userObj!["user"]["firstName"];
      lastName.text = userObj!["user"]["lastName"];
      getUserProfileImg();
    }
  }

  @override
  void initState() {
    getUserDetails();
  }
}
