import 'package:flutter/material.dart';
import 'package:ems_mob_app/controllers/app_layout_controller.dart';
import 'package:ems_mob_app/routes/app_pages.dart';
import 'package:get/get.dart';

class RootPage extends GetView<RootController> {
  const RootPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetRouterOutlet.builder(
      builder: (context, delegate, current) {
        return Scaffold(
          body: GetRouterOutlet(
            initialRoute: AppPages.initial,
          ),
        );
      },
    );
  }
}
