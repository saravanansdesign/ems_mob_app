import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:ems_mob_app/services/auth_api.dart';
import 'modules/utils/constant_variables.dart';
import 'routes/app_pages.dart';
import 'package:get_storage/get_storage.dart';
import 'package:ems_mob_app/modules/utils/themes.dart';
import 'package:ems_mob_app/modules/utils/globals.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';

String initRoute = AppPages.login;

const AndroidNotificationChannel channel = AndroidNotificationChannel(
    "high_importance_channel",
    "High Importance Notifications",
    importance: Importance.high,
    playSound: true);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print("A Bg message just showed up: ${message.messageId}");
}

void main() async {
  await GetStorage.init();
  WidgetsFlutterBinding.ensureInitialized();

  themeDark = GetStorage().read<bool>('themeDark') ?? false;
  AuthApiService authService = AuthApiService();
  UtilService utils = UtilService();

  var userObj = await authService.getUserObj();
  if( userObj != null){
    initRoute = AppPages.dashboard;
  }else{
    initRoute = AppPages.login;
  }

  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true
  );


  // **********************************************
  // FCM Push Notification - Start
  // **********************************************

  FirebaseMessaging.instance.getToken().then((token) {
    SharedPreferences.getInstance().then((prefs) async {
      log("fcmToken=> $token");
      prefs.setString("fcmToken", token!);
      var userObj = prefs.getString("userObj");

      if (userObj != null && userObj != "") {
        print("===============================");
        print("Firebase token");
        print("===============================");
        print(token);
      }
    });
  });

  Stream<String> fcmStream = FirebaseMessaging.instance.onTokenRefresh;
  fcmStream.listen((token) {
    SharedPreferences.getInstance().then((prefs) async {
      log("fcmToken=> $token");
      prefs.setString("fcmToken", token);
      var userObj = prefs.getString("userObj");

      if (userObj != null && userObj != "") {
        print("===============================");
        print("Firebase token");
        print("===============================");
        print(token);
      }
    });
  });

  FirebaseMessaging.onMessage.listen((RemoteMessage message) {
    print("===============================");
    print("Message Received...!");
    print("===============================");
    print(message);

    RemoteNotification? notification = message.notification;
    AndroidNotification? android = message.notification?.android;
    if (notification != null && android != null){
      flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              color: Colors.blue,
              playSound: true,
              icon: "@mipmap/ic_launcher",
            ),
          )
      );
    }
  });

  FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
    print("===============================");
    print("A new onMessageOpenedApp event was published!");
    print("===============================");
    print(message);
  });

  // **********************************************
  // FCM Push Notification - End
  // **********************************************

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: "YesLedgers",
        debugShowCheckedModeBanner: false,
        defaultTransition: Transition.native,
        getPages: AppPages.routes,
        theme: ThemeData(
          fontFamily: AppTheme.font,
        ),
      darkTheme: Themes.light,
        builder: EasyLoading.init(),
        initialRoute: initRoute,
    );
  }
}
