import 'dart:convert';
import 'package:get/get.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:ems_mob_app/services/auth_api.dart';
import 'package:ems_mob_app/services/schedules_api.dart';
import 'package:ems_mob_app/modules/utils/log.dart';
import 'package:ems_mob_app/app-conf.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class SchedulesController extends GetxController {

  SchedulesApiService schedulesApiService = SchedulesApiService();
  AuthApiService authApiService = AuthApiService();
  UtilService utils = UtilService();

  var userObj;
  var customerObj;
  @override
  void onInit() {
    super.onInit();
  }

  getScheduleList(selectedState,sort) async {
    userObj = await authApiService.getUserObj();
    customerObj = await authApiService.getCustomerObj();
    var reqData = {
      "query": {
        "bool": {
          "must": [
            {
              "match": {"domainKey": Env.conf["domain_key"]}
            },
            {
              "match": {"mobile": customerObj['mobile']}
            }

          ],
          "should": [],
          "filter": {
            "range": {
              "scheduledmillis": {
                "lte": utils.dhzTime()["toTime"],
                "gte": utils.dhzTime()["fromTime"]
              }
            }
          }
        }
      },
      "aggs": {
        "state": {
          "terms": {
            "field": "state",
            "size": 10000,
            "order": {"_key": "asc"}
          }
        }
      },
      "sort": [
        {
          "updatedtime": {"order": sort}
        }
      ],
      "size": 1000,
      "from": 0
    };

        if(selectedState == "HISTORY"){
          ((reqData["query"]! as dynamic)["bool"]["should"] as List).add({
            "match": {"state": "CANCELLED"}
          });
          ((reqData["query"]! as dynamic)["bool"]["should"] as List).add({
            "match": {"state": "COMPLETED"}
          });
          ((reqData["query"]! as dynamic)["bool"]["should"] as List).add({
            "match": {"state": "EXPIRED"}
          });
             ((reqData["query"]! as dynamic)["bool"])["minimum_should_match"]=1;

        }
        else if(selectedState!=""){
          ((reqData["query"]! as dynamic)["bool"]["must"] as List).add({
            "match": {"state": selectedState}
            });
        }
    var inputObj = {
      "method": "GET",
      "extraPath": "",
      "query": jsonEncode(reqData),
      "params": []
    };
    var response = await schedulesApiService.getScheduleListAPI(inputObj);
    var list = response["result"]["data"];
    if(selectedState == "") {

      for(var i=0;i<response["result"]["aggregations"]["state"]["buckets"].length;i++){
        if(response["result"]["aggregations"]["state"]["buckets"][i]["key"]=="SCHEDULED"){
        list["scheduled_count"] = response["result"]["aggregations"]["state"]["buckets"][i]["doc_count"];
        }
        if(response["result"]["aggregations"]["state"]["buckets"][i]["key"]=="IN-PROCESS"){
          list["inprocess_count"] = response["result"]["aggregations"]["state"]["buckets"][i]["doc_count"];
        }
        if(response["result"]["aggregations"]["state"]["buckets"][i]["key"]=="EXPIRED"){
          list["expired_count"] = response["result"]["aggregations"]["state"]["buckets"][i]["doc_count"];
        }
        if(response["result"]["aggregations"]["state"]["buckets"][i]["key"]=="COMPLETED"){
          list["completed_count"] = response["result"]["aggregations"]["state"]["buckets"][i]["doc_count"];
        }
        if(response["result"]["aggregations"]["state"]["buckets"][i]["key"]=="CANCELLED"){
          list["cancelled_count"] = response["result"]["aggregations"]["state"]["buckets"][i]["doc_count"];
        }
      }
    }
    return list;
  }


}