import 'dart:convert';
import 'package:get/get.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:ems_mob_app/services/auth_api.dart';
import 'package:ems_mob_app/services/dashboard_api.dart';
import 'package:ems_mob_app/services/shuttle_api.dart';
import 'package:ems_mob_app/modules/utils/log.dart';
import 'package:ems_mob_app/app-conf.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class ShuttleController extends GetxController {

  DashboardApiService dashboardApiService = DashboardApiService();
  ShuttleApiService shuttleApiService = ShuttleApiService();
  AuthApiService authApiService = AuthApiService();
  UtilService utils = UtilService();
  var customerObj;

  getShuttleList(reqObj) async{

    customerObj = await authApiService.getCustomerObj();

    var query1 = {
      "query": {
        "bool": {
          "must": [
            {
              "match": {"domainKey": Env.conf["domain_key"]}
            }
          ]
        }
      },
      "sort": [
        {
          "updatedtime": {"order": "desc"}
        }
      ],
      "size": 100,
      "from": 0
    };

    var queryObj = {
      "method": "GET",
      "extraPath": "",
      "query": jsonEncode(query1),
      "params": []
    };

    var resObj = await shuttleApiService.getOnServiceShuttleListAPI(queryObj);
    var response = {
      "status" : false,
      "result" : [],
    };
    if(resObj["status"]){
      response["status"] = true;
      response["result"] = resObj["result"]["data"]["data"];
    }else{
      response["status"] = false;
      response["result"]  = [];
    }

    return response;
  }

  getSingleShuttleDetails(reqObj) async{

    customerObj = await authApiService.getCustomerObj();

    var query1 = {
      "query": {
        "bool": {
          "must": [{
            "match": {
              "domainKey": Env.conf["domain_key"]
            }
          },
            {
              "match_phrase": {
                "shuttle_id": reqObj["shuttle_id"]
              }
            }]
        }
      },
      "sort": [
        {
          "seat_no": {"order": "asc"}
        }
      ],
      "size": 100,
      "from": 0
    };

    print("query1----------------");
    print(jsonEncode(query1));

    var queryObj = {
      "method": "GET",
      "extraPath": "",
      "query": jsonEncode(query1),
      "params": []
    };

    var resObj = await shuttleApiService.getSingleShuttleDetailsAPI(queryObj);
    var response = {
      "status" : false,
      "result" : [],
    };
    if(resObj["status"]){
      response["status"] = true;
      response["result"] = resObj["result"]["data"]["data"];
    }else{
      response["status"] = false;
      response["result"]  = [];
    }

    return response;
  }
}