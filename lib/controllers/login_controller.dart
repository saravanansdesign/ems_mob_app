import 'dart:convert';
import 'package:get/get.dart';
import 'package:ems_mob_app/services/auth_api.dart';
import 'package:ems_mob_app/views/pages/login/login_page.dart';
import '../app-conf.dart';

class LoginController extends GetxController {

  AuthApiService authService = AuthApiService();
  LoginPage loginForm = LoginPage();

  userLogin(inputObj) async {

    var res = await authService.loginAPI(inputObj);
    return res;
  }

  userSignUp(dynamic inputObj) async {
    var res = await authService.userCreateAPI(inputObj);
    return res;
  }

  forgotPasswordCtrl(String email) async {

    var inputObj = {
      "email":email
    };

    var response = await authService.forgotPasswordAPI(inputObj);
    return response;
  }
}