import 'dart:convert';
import 'package:get/get.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:ems_mob_app/services/auth_api.dart';
import 'package:ems_mob_app/services/entry_api.dart';
import 'package:ems_mob_app/modules/utils/log.dart';
import 'package:ems_mob_app/app-conf.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class EntryController extends GetxController {

  EntryApiService entryApiService = EntryApiService();
  AuthApiService authApiService = AuthApiService();
  UtilService utils = UtilService();

  var userObj;
  var customerObj;
  @override
  void onInit() {
    super.onInit();
  }

  getAllEntryList() async {

    userObj = await authApiService.getUserObj();
    customerObj = await authApiService.getCustomerObj();
    /*var reqData = {
      "query": {
        "bool": {
          "must": [
            {
              "match": {"domainKey": Env.conf["domain_key"]}
            },
            {
              "match": {"mobile": customerObj['mobile']}
            }

          ],
          "should": [],
          "filter": {
            "range": {
              "scheduledmillis": {
                "lte": utils.dhzTime()["toTime"],
                "gte": utils.dhzTime()["fromTime"]
              }
            }
          }
        }
      },
      "aggs": {
        "state": {
          "terms": {
            "field": "state",
            "size": 10000,
            "order": {"_key": "asc"}
          }
        }
      },
      "sort": [
        {
          "updatedtime": {"order": sort}
        }
      ],
      "size": 1000,
      "from": 0
    };*/

    var reqData = {
      "query": {
        "bool": {
          "must": [
            {
              "match": {"user_id": userObj["username"]}
            },
            {
              "match_phrase": {"ledger_name": "April 2022"}
            }
          ],
          "should": []
        }
      },
      "sort": [
        {
          "expense_ts": {"order": "desc"}
        }
      ],
      "size": 10,
      "from": 0
    };

    var response = await entryApiService.getEntryListAPI(reqData);

    var obj = {};

    if(response["status"]){
      obj = {
        "status" : true,
        "data" : response["result"]["data"]["data"]
      };
    }else{
      obj = {
        "status" : false,
        "data" : response["message"]
      };
    }
    return obj;
  }

  getAllLedgersList() async {

    userObj = await authApiService.getUserObj();

    var reqData = {
      "query": {
        "bool" : {
          "must" : [
            {
              "match_phrase": {
                "user_id": userObj["username"]
              }
            }
          ]
        }
      },
      "sort": [{"updated_time": {"order": "asc"}}],
      "size": 100,
      "from": 0
    };
    print("reqData----------");
    print(reqData);
    var response = await entryApiService.getLedgerListAPI(reqData);

    var obj = {};

    if(response["status"]){
      obj = {
        "status" : true,
        "data" : response["result"]["data"]["data"]
      };
    }else{
      obj = {
        "status" : false,
        "data" : response["message"]
      };
    }
    return obj;
  }

  getCounts() async {

    userObj = await authApiService.getUserObj();
    customerObj = await authApiService.getCustomerObj();

    var reqData = {
      "ledger_name": "April 2022",
      "user_id": userObj["username"]
    };

    var response = await entryApiService.getSingleLedgerCountsAPI(reqData);

    var obj = {};

    if(response["status"]){

      var income = response["result"]["income"];
      var expense = response["result"]["expense"];
      var balance = income - expense;

      obj = {
        "status" : true,
        "result" : {
          "income" : income,
          "expense" : expense,
          "balance" : balance
        }
      };
    }else{
      obj = {
        "status" : false,
        "data" : response["message"]
      };
    }
    return obj;
  }

  addEntry() async {

  }
}