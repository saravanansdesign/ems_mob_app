import 'dart:async';
import 'dart:convert';
import 'package:get/get_navigation/src/routes/route_middleware.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:ems_mob_app/modules/utils/log.dart';
import 'package:ems_mob_app/app-conf.dart';
import 'package:ems_mob_app/services/auth_api.dart';

class EntryApiService extends GetMiddleware{
  String errorMessage = "";
  AuthApiService authService = AuthApiService();
  UtilService utils = UtilService();

  Future getEntryListAPI(reqData) async {

    final url = "${Env.conf["api_base_path"]}/expense/list";
    Map<String, String> headers = {
      'Content-Type': 'application/json'
    };

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(reqData));

    var resObj = jsonDecode(response.body);

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      resultObj["result"] = resObj["result"];
      resultObj["status"] = true;
    } else {
      resultObj["message"] = resObj["message"];
      resultObj["status"] = false;
    }

    return resultObj;
  }

  Future getLedgerListAPI(reqData) async {

    final url = "${Env.conf["api_base_path"]}/ledger/list";
    Map<String, String> headers = {
      'Content-Type': 'application/json'
    };

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(reqData));

    var resObj = jsonDecode(response.body);

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      resultObj["result"] = resObj["result"];
      resultObj["status"] = true;
    } else {
      resultObj["message"] = resObj["message"];
      resultObj["status"] = false;
    }

    return resultObj;
  }

  Future getSingleLedgerCountsAPI(reqData) async {

    final url = "${Env.conf["api_base_path"]}/dashboard/ledger-count";
    Map<String, String> headers = {
      'Content-Type': 'application/json'
    };

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(reqData));

    var resObj = jsonDecode(response.body);

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      resultObj["result"] = resObj["result"];
      resultObj["status"] = true;
    } else {
      resultObj["message"] = resObj["message"];
      resultObj["status"] = false;
    }

    return resultObj;
  }
}