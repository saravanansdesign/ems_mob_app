import 'dart:async';
import 'dart:convert';
import 'package:get/get_navigation/src/routes/route_middleware.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:ems_mob_app/modules/utils/log.dart';
import 'package:ems_mob_app/app-conf.dart';
import 'package:ems_mob_app/services/auth_api.dart';

class SettingsApiService extends GetMiddleware{
  String errorMessage = "";
  AuthApiService authService = AuthApiService();
  UtilService utils = UtilService();

  Future updateHostNotificationAPI(reqData, rid) async {

    print("rid reqData--------------------");
    print(rid);
    print(reqData);

    final url = "${Env.conf["api_base_path"]}/record/insert/static/${Env.conf["api_token"]}/10005/$rid";
    Map<String, String> headers = {
      'Content-Type': 'text/plain'
    };

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(reqData));

    print("response.body--------------------");
    print(response.body);

    var resObj = jsonDecode(response.body);
    var resData = utils.searchResultFormatter(resObj);

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      resultObj["result"] = resData;
      resultObj["status"] = true;
    } else {
      resultObj["message"] = resData;
      resultObj["status"] = false;
    }

    return resultObj;
  }
}