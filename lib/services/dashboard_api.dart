import 'dart:async';
import 'dart:convert';
import 'package:get/get_navigation/src/routes/route_middleware.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:ems_mob_app/modules/utils/log.dart';
import 'package:ems_mob_app/app-conf.dart';
import 'package:ems_mob_app/services/auth_api.dart';

class DashboardApiService extends GetMiddleware{
  String errorMessage = "";
  AuthApiService authService = AuthApiService();
  UtilService utils = UtilService();

  Future getAllVisitorsCountAPI(reqData) async {

    // final url = "${Env.conf["api_base_path"]}/elastic/search/query/${Env.conf["api_token"]}/RECORD/?specId=10002";
    final url = "${Env.conf["api_base_path"]}/elastic/search/query/${Env.conf["api_token"]}";
    Map<String, String> headers = {
      'Content-Type': 'application/json'
    };

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(reqData));

    var resObj = jsonDecode(response.body);
    var resData = utils.searchResultFormatter(resObj);

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      resultObj["result"] = resData;
      resultObj["status"] = true;
    } else {
      resultObj["message"] = resData;
      resultObj["status"] = false;
    }

    return resultObj;
  }

  Future getInsideExitVisitorAPI(reqData) async {

    final url = "${Env.conf["api_base_path"]}/elastic/search/query/${Env.conf["api_token"]}";
    Map<String, String> headers = {
      'Content-Type': 'application/json'
    };

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(reqData));

    var resObj = jsonDecode(response.body);
    var resData = utils.searchResultFormatter(resObj);

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      resultObj["result"] = resData;
      resultObj["status"] = true;
    } else {
      resultObj["message"] = resData;
      resultObj["status"] = false;
    }

    return resultObj;
  }

  Future getScheduledVisitsAPI(reqData) async {

    final url = "${Env.conf["api_base_path"]}/elastic/search/query/${Env.conf["api_token"]}";
    Map<String, String> headers = {
      'Content-Type': 'application/json'
    };

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(reqData));

    var resObj = jsonDecode(response.body);
    var resData = utils.searchResultFormatter(resObj);

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      resultObj["result"] = resData;
      resultObj["status"] = true;
    } else {
      resultObj["message"] = resData;
      resultObj["status"] = false;
    }

    return resultObj;
  }

  Future getRecentVisitorsListAPI(reqData) async {

    final url = "${Env.conf["api_base_path"]}/elastic/search/query/${Env.conf["api_token"]}/RECORD/?specId=10002";
    Map<String, String> headers = {
      'Content-Type': 'application/json'
    };

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(reqData));

    var resObj = jsonDecode(response.body);
    var resData = utils.searchResultFormatter(resObj);

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      resultObj["result"] = resData;
      resultObj["status"] = true;
    } else {
      resultObj["message"] = resData;
      resultObj["status"] = false;
    }

    return resultObj;
  }
}