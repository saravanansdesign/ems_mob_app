import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/routes/route_middleware.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ems_mob_app/modules/utils/constant_variables.dart';
import 'package:ems_mob_app/modules/utils/common.dart';
import 'package:ems_mob_app/modules/utils/log.dart';
import 'package:ems_mob_app/routes/app_pages.dart';
import 'package:ems_mob_app/services/auth_guard.dart';
import 'package:dio/dio.dart';
import 'package:mime/mime.dart';
import '../app-conf.dart';

class AuthApiService extends GetMiddleware{
  var userObj;
  var customerObj;
  String errorMessage = "";
  UtilService utils = UtilService();

  Future getUserObj() async{
    var _saved;
    var _prefs = await SharedPreferences.getInstance();
    _saved = _prefs.getString("userObj");
    if (_saved != null) {
      userObj = json.decode(_saved);
      Env.conf["api_token"] = userObj["token"];
    }
    return userObj;
  }

  Future createUserAccount() async{
    var _saved;
    var _prefs = await SharedPreferences.getInstance();
    _saved = _prefs.getString("userAccounts");
    if (_saved != null) {
      userObj = json.decode(_saved);
      Env.conf["api_token"] = userObj["token"];
    }
    return userObj;
  }

  Future getCustomerObj() async{
    var _saved;
    var _prefs = await SharedPreferences.getInstance();
    _saved = _prefs.getString("customerObj");
    if (_saved != null) {
      customerObj = json.decode(_saved);
    }
    return customerObj;
  }

  void loadSettings() async {

    var _prefs = await SharedPreferences.getInstance();

    try {
      Env.conf["api_base_path"] = _prefs.getString("api_base_path") ?? "";
    } catch (e) {
      log(e);
      Env.conf["api_base_path"] = "";
    }

    try {
      var _saved = _prefs.getString("userObj");
      log("Saved: $_saved");
      if (_saved != null) {
        userObj = json.decode(_saved);
      }
    } catch (e) {
      log("User Not Found: $e");
    }
  }

  Future loginAPI(reqData) async {

    final url = "${Env.conf["api_base_path"]}/auth/login";
    Map<String, String> headers = {
      'Content-Type': 'application/json'
    };

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(reqData));

    var resObj = jsonDecode(response.body);

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      resultObj["result"] = resObj["result"];
      resultObj["status"] = true;

      print("result-----------------");
      print(resObj["result"]);

      SharedPreferences.getInstance().then((prefs) {
        var _save = json.encode(resObj["result"]);
        log("userObj=> $_save");
        prefs.setString("userObj", _save);
      });
    } else {
      resultObj["message"] = resObj["message"];
      resultObj["status"] = false;
    }

    return resultObj;
  }

  Future platformUserCreateAPI(userObj) async {

    final url = "${Env.conf["api_base_path"]}/user/upsert/${Env.conf["domain_key"]}:${Env.conf["api_key"]}";
    Map<String, String> headers = {'Content-Type': 'application/json'};

    final response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: jsonEncode(userObj),
    );

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      Env.conf["api_token"] = jsonData["token"];
      resultObj["status"] = true;
      resultObj["result"] = jsonData;
    } else {
      resultObj = {
        "status": false,
        "result": response.body,
      };
    }
    return resultObj;
  }

  Future userCreateAPI(reqData) async {

    final url = "${Env.conf["api_base_path"]}/auth/signup";
    Map<String, String> headers = {
      'Content-Type': 'application/json'
    };

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(reqData));

    var resObj = jsonDecode(response.body);

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      resultObj["result"] = resObj["result"];
      resultObj["status"] = true;
    } else {
      resultObj["message"] = resObj["message"];
      resultObj["status"] = false;
    }

    return resultObj;
  }

  Future isUserExistAPI(email) async {
    final url = "${Env.conf["api_base_path"]}/user/get/${Env.conf["domain_key"]}:${Env.conf["api_key"]}/$email";

    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);

    var jsonData = json.decode(response.body);

    var res = false;
    if(jsonData["email"] != null){
      res = true;  // User Already Exist
    }

    return res;
  }

  Future forgotPasswordAPI(reqData) async {

    final url = "${Env.conf["api_base_path"]}/call/v2/execute/rule/${Env.conf["domain_key"]}:${Env.conf["api_key"]}";
    Map<String, String> headers = {
      'Content-Type': 'application/json'
    };

    final response = await http.post(Uri.parse(url), headers: headers, body: jsonEncode(reqData));

    var resObj = jsonDecode(response.body);
    var resData = utils.searchResultFormatter(resObj);

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);

      if(jsonData["status"]){
        resultObj["status"] = true;
        resultObj["result"] = jsonData["response"];
      }else{
        resultObj["status"] = false;
        resultObj["result"] = jsonData["response"];
      }
    } else {

      resultObj = {
        "status": false,
        "result": response.body,
      };
    }

    return resultObj;
  }

  Future getCustomerDetailsAPI(reqData) async {

    final url = "${Env.conf["api_base_path"]}/elastic/search/query/${Env.conf["api_token"]}/RECORD/?specId=${AppTable.customerMasterTable}";
    Map<String, String> headers = {
      'Content-Type': 'application/json'
    };

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(reqData));

    var resObj = jsonDecode(response.body);
    var resData = utils.searchResultFormatter(resObj);

    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      resultObj["result"] = resData;
      resultObj["status"] = true;

      var customerData;
      if(resData["data"] != null){
        customerData = (resData["data"] as dynamic)["data"];
      }

      if(customerData.length > 0){
        SharedPreferences.getInstance().then((prefs) {
          var _save = json.encode(customerData[0]);
          log("customerObj=> $_save");
          prefs.setString("customerObj", _save);
        });
      }else{
        resultObj["message"] = "Host details not found";
        resultObj["status"] = false;
      }
    } else {
      resultObj["message"] = resData;
      resultObj["status"] = false;
    }

    return resultObj;
  }

  Future getDomainDetailsAPI(domainKey) async {
    Map<String, String> headers = {'Content-Type': 'application/json'};
    var url =
        "${Env.conf["api_base_path"]}/domain/login/switch/${Env.conf["api_token"]}/$domainKey";

    final response = await http.get(Uri.parse(url), headers: headers);
    var jsonData = json.decode(response.body);
    var resultObj = {};

    if (response.statusCode == 200) {
      Env.conf["api_token"] = jsonData["token"];
      Env.conf["DOMAIN_KEY"] = jsonData["domainKey"];
      Env.conf["API_KEY"] = jsonData["apiKey"];
      resultObj = {
        "status": true,
        "result": jsonData,
      };

      SharedPreferences.getInstance().then((prefs) {
        var _save = json.encode(jsonData);
        log("userObj=> $_save");
        prefs.setString("userObj", _save);
      });
    } else {
      resultObj = {
        "status": false,
        "result": jsonData,
      };
    }

    return resultObj;
  }

  Future getVersion() async {
    Map<String, String> headers = {'Content-Type': 'application/json'};
    var url = "${Env.conf["api_base_path"]}/cluster/statistics";

    final response = await http.get(Uri.parse(url), headers: headers);
    var jsonData = json.decode(response.body);
    var resultObj = {};

    if (response.statusCode == 200) {
      Env.conf["platform_version"] = jsonData["version"];
      resultObj = {
        "status": true,
        "result": jsonData,
      };
    } else {
      resultObj = {
        "status": false,
        "result": jsonData,
      };
    }

    return resultObj;
  }

  Future getUserProperty(email) async {
    Map<String, String> headers = {'Content-Type': 'application/json'};
    var url = "${Env.conf["api_base_path"]}/user/property/get/${Env.conf["api_token"]}/$email/user.picture";

    final response = await http.get(Uri.parse(url), headers: headers);
    var jsonData = json.decode(response.body);
    var resultObj = {};

    if (response.statusCode == 200) {
      resultObj = {
        "status": true,
        "result": json.decode(jsonData["value"])["picture"],
      };
    } else {
      resultObj = {
        "status": false,
        "result": null,
      };
    }

    return resultObj;
  }

  Future imageUploadAPI(file) async {
    Map<String, String> headers = {'Content-Type': 'application/json'};
    // var url = "${Env.conf["api_base_path"]}/user/property/get/${Env.conf["api_token"]}/$email/user.picture";
    var url =
        "${Env.conf["api_base_path"]}/files/upload/${Env.conf["api_token"]}?id=${Env.conf["user_pro_img_id"]}";
    var formData = FormData.fromMap({
      'binfile': await MultipartFile.fromFile(file.path, filename: file.name),
      'mediaType': lookupMimeType(file.path),
      'tags': "Profile Picture",
      'description': ""
    });
    var response = await Dio().post(url, data: formData);
    var jsonData = response.data;
    var resultObj = {};

    if (response.statusCode == 200) {
      resultObj = {
        "status": true,
        "result": jsonData,
      };
    } else {
      resultObj = {
        "status": false,
        "result": jsonData,
      };
    }

    return resultObj;
  }

  Future uploadImgFile(file) async {
    var stringTxt =
        "https://dev.boodskap.io/api/files/upload/9051e38f-44bf-4fd3-8af0-7ae5c6c0dd74?id=4a02a73c-85ba-11e8-adc0-fa7ae01bbebc";
  }

  Future updateProfilePicture(username, password) async {
    final url = "${Env.conf["api_base_path"]}/user/property/upsert/${Env.conf["api_token"]}";
    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.post(Uri.parse(url), headers: headers);

    var jsonData = json.decode(response.body);
    SharedPreferences.getInstance().then((prefs) {
      var _save = json.encode(jsonData.toJson());
      log("userObj=> $_save");
      prefs.setString("userObj", _save);
    });
    return jsonData;
  }

  Future userProfileUpdate(inputObj) async {
    final url = "${Env.conf["api_base_path"]}/user/upsert/${Env.conf["api_token"]}";
    Map<String, String> headers = {'Content-Type': 'application/json'};

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(inputObj));

    var jsonData = json.decode(response.body);
    return jsonData;
  }

  Future getUserProfile(email) async {
    final url = "${Env.conf["api_base_path"]}/user/get/${Env.conf["api_token"]}/$email";

    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);

    var jsonData = json.decode(response.body);

    return jsonData;
  }

  Future getDashboardList(email) async {
    final url =
        "${Env.conf["api_base_path"]}/domain/property/get/${Env.conf["api_token"]}/domain.mobile.dashboard.list";

    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);

    var result = json.decode(response.body);
    var jsonData = {};

    if (result!.length > 0) {
      jsonData["result"] = json.decode(response.body);
      jsonData["status"] = true;
    } else {
      jsonData["message"] = json.decode(response.body);
      jsonData["status"] = false;
    }

    return jsonData;
  }

  Future getDomainProperty(name) async {
    final url =
        "${Env.conf["api_base_path"]}/domain/property/get/${Env.conf["api_token"]}/${name}";
    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);
    var resultObj = {};
    var jsonData = json.decode(response.body);

    if (response.statusCode == 200) {
      resultObj["status"] = true;
      resultObj["result"] = jsonData;
    } else {
      resultObj["status"] = false;
      resultObj["result"] = jsonData;
    }

    return resultObj;
  }

  Future getSingleDashboardWidgetsAPI(dashProperty) async {
    final url =
        "${Env.conf["api_base_path"]}/domain/property/get/${Env.conf["api_token"]}/${dashProperty}";

    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);

    var jsonData = json.decode(response.body);

    return jsonData;
  }

  Future getSingleWidgetAPI(dashProperty) async {
    final url =
        "${Env.conf["api_base_path"]}/domain/property/get/${Env.conf["api_token"]}/${dashProperty}";

    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);

    var jsonData = json.decode(response.body);

    return jsonData;
  }

  Future getWidgetsList(dashboardId) async {
    final url =
        "${Env.conf["api_base_path"]}/domain/property/get/${Env.conf["api_token"]}/mobile.${dashboardId}";

    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);

    var jsonData = json.decode(response.body);

    return jsonData;
  }

  // create Schedule
  Future createSchedule(data) async {
    final url = "${Env.conf["api_base_path"]}/record/insert/dynamic/${Env.conf["api_token"]}/10004";
    Map<String, String> headers = {'Content-Type': 'text/plain'};
    final response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: jsonEncode(data),
    );
    var resultObj = {
      "status": false,
      "result":{},
    };

    if (response.statusCode == 200) {
      resultObj = {
        "status": true,
        "result": json.decode(response.body),
      };
    }
    else{
      resultObj = {
        "status" : false,
        "result" : json.decode(response.body),
      };
    }
    return resultObj;
  }

  //msg mail
  Future sendScheduleMsg(datas,type) async{
    if(type == "CREATE"){
      datas['vist'] = "SCHEDULE";
    } else if(type == "EDIT"){
      datas['vist'] = "RE-SCHEDULE";
    } else if(type == "CANCEL"){
    datas['vist'] = "CANCEL";
    }
    var data = {
      "sessionId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "namedrule": "schedule-visit",
      "scriptArgs": datas
    };
    final url = "${Env.conf["api_base_path"]}/call/v2/execute/rule/${Env.conf["api_token"]}";
    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: jsonEncode(data),
    );
    var resultObj = {
      "status": false,
      "result":{},
    };
    if (response.statusCode == 200) {
      resultObj = {
        "status": true,
        "result": json.decode(response.body),
      };
    }
    else{
      resultObj = {
        "status" : false,
        "result" : json.decode(response.body),
      };
    }
    return resultObj;

  }

  // edit Schedule
  Future editSchedule(data,rkey) async {
    final url = "${Env.conf["api_base_path"]}/record/insert/static/${Env.conf["api_token"]}/10004/"+rkey;
    Map<String, String> headers = {'Content-Type': 'text/plain'};
    final response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: jsonEncode(data),
    );
    var resultObj = {
      "status": false,
      "result":{},
    };
    if (response.statusCode == 200) {
      resultObj = {
        "status": true,
        "result": json.decode(response.body),
      };
    }
    else{
      resultObj = {
        "status" : false,
        "result" : json.decode(response.body),
      };
    }
    return resultObj;
  }

  // delete Schedule
  Future deleteSchedule(data,rkey) async {
    data.remove("_id");
    final url = "${Env.conf["api_base_path"]}/record/insert/static/${Env.conf["api_token"]}/10004/"+rkey;
    Map<String, String> headers = {'Content-Type': 'text/plain'};
    final response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: jsonEncode(data),
    );
    var resultObj = {
      "status": false,
      "result":{},
    };
    if (response.statusCode == 200) {
      resultObj = {
        "status": true,
        "result": json.decode(response.body),
      };
    }
    else{
      resultObj = {
        "status" : false,
        "result" : json.decode(response.body),
      };
    }
    return resultObj;
  }

  Future createVisitor(data) async {
    final url = "${Env.conf["api_base_path"]}/record/insert/dynamic/${Env.conf["api_token"]}/10003";
    Map<String, String> headers = {'Content-Type': 'text/plain'};
    final response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: jsonEncode(data),
    );
    var resultObj = {
      "status": false,
      "result":{},
    };

    if (response.statusCode == 200) {
      resultObj = {
        "status": true,
        "result": json.decode(response.body),
      };
    }
    else{
      resultObj = {
        "status" : false,
        "result" : json.decode(response.body),
      };
    }
    return resultObj;
  }

  Future editVisitor(data,rkey) async {
    data.remove("_id");
    final url = "${Env.conf["api_base_path"]}/record/insert/static/${Env.conf["api_token"]}/10003/"+rkey;
    Map<String, String> headers = {'Content-Type': 'text/plain'};
    final response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: jsonEncode(data),
    );
    var resultObj = {
      "status": false,
      "result":{},
    };
    if (response.statusCode == 200) {
      resultObj = {
        "status": true,
        "result": json.decode(response.body),
      };
    }
    else{
      resultObj = {
        "status" : false,
        "result" : json.decode(response.body),
      };
    }
    return resultObj;
  }

  Future deleteVisitor(data,rkey) async {
    data.remove("_id");
    final url = "${Env.conf["api_base_path"]}/record/delete/${Env.conf["api_token"]}/10003/"+rkey;
    final response = await http.delete(
      Uri.parse(url),
    );
    var resultObj = {
      "status": false,
      "result":{},
    };
    if (response.statusCode == 200) {
      resultObj = {
        "status": true,
        "result": json.decode(response.body),
      };
    }
    else{
      resultObj = {
        "status" : false,
        "result" : json.decode(response.body),
      };
    }
    return resultObj;
  }

  Future visitorList(data,visitorCheck) async {
    var name = data["name"];
    var number = data["number"];
    var mail = data["mail"];
    var org = data["org"];
    if (visitorCheck) {

      var query = {
        "query": {
          "bool": {
            "must": [
              {
                "match": {"domainKey": Env.conf["domain_key"]}
              },
              {
                "match": {"mobile": number}
              },
            ],
          }
        },
        "size": 1000,
        "sort": []
      };
      var data = {
        "method": "GET",
        "extraPath": "",
        "query": jsonEncode(query),
        "params": []
      };
      final url = "${Env.conf["api_base_path"]}/elastic/search/query/${Env
          .conf["api_token"]}/RECORD/?specId=10003";
      Map<String, String> headers = {'Content-Type': 'application/json'};
      final response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: jsonEncode(data),
      );
      var resultObj = {
        "status": false,
        "result": {},
      };

      if (response.statusCode == 200) {
        resultObj = {
          "status": true,
          "result": json.decode(response.body),
        };

        if(visitorCheck){
        await getCustomerObj();
        if(jsonDecode((resultObj["result"]! as dynamic)["result"])["hits"]["total"]["value"] == 0) {
          var visitorObj = {
            "vname": name,
            "vemail": mail,
            "vorganization": org,
            "vmobile": number,
            "mobile": number,
            "vphoto": "",
            "createdtime": new DateTime.now().millisecondsSinceEpoch,
            "updatedtime": new DateTime.now().millisecondsSinceEpoch,
            "createdby": customerObj['hemail'],
            "updatedby": customerObj['hemail']
          };
          createVisitor(visitorObj).then((result) async {
            if (result['status']) {

            }
          });
        }
        }


      }
      else {
        resultObj = {
          "status": false,
          "result": json.decode(response.body),
        };
      }


       return resultObj;
    }
    else {
    var query = {
      "query": {
        "bool": {
          "must": [
            {
              "match": {"domainKey": Env.conf["domain_key"]}
            },
          ],


          "should": [
            {
              "wildcard": {
                "vname": "*${name.toString().toLowerCase()}*"
              }
            },
            {
              "wildcard": {
                "vname": "*${name.toString().toUpperCase()}*"
              }
            },
            {
              "wildcard": {
                "vname": "*${utils.getCap(name)}*"
              }
            }
          ],
          "minimum_should_match": 1,
        }
      },
      "size": 1000,
      "sort": []
    };

    var data = {
      "method": "GET",
      "extraPath": "",
      "query": jsonEncode(query),
      "params": []
    };
    final url = "${Env.conf["api_base_path"]}/elastic/search/query/${Env
        .conf["api_token"]}/RECORD/?specId=10003";
    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: jsonEncode(data),
    );
    var resultObj = {
      "status": false,
      "result": {},
    };

    if (response.statusCode == 200) {
      resultObj = {
        "status": true,
        "result": json.decode(response.body),
      };
    }
    else {
      resultObj = {
        "status": false,
        "result": json.decode(response.body),
      };
    }
    return resultObj;
  }
  }


  // Logout
  Future<void> logout() async {
    // Simulate a future for response after 1 second.
    SharedPreferences.getInstance().then((prefs) {
      prefs.clear();
    });
  }

  void fToast(msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  @override
  void initState() {
    loadSettings();
  }
}